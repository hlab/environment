import json
import os

from os.path import expanduser
from pathlib import Path 

from shutil import copyfile

CONF_DIR = os.path.dirname(os.path.abspath(__file__))

class Config:

    def __init__(self, config_file_name="hlab.json", config_folder_path=None):

        template_path = os.path.join(CONF_DIR, config_file_name)

        if not config_folder_path:
            config_folder_path = expanduser(os.path.join("~", ".hlab"))
            
        config_file_path = os.path.join(config_folder_path, config_file_name)


        if not os.path.exists(config_folder_path):
            print( "Creating file path %s/" % config_folder_path )
            Path(config_folder_path).mkdir(parents=True)
        if not os.path.exists(config_file_path):
            print( "Creating file %s" % config_file_path )
            copyfile(template_path, config_file_path)
        else:
            print("Using hlab config in {}".format(config_file_path))
        
        self.path = config_file_path
        
    def __repr__(self):
        return str(self.load_config(self.path))
    

    @property
    def current(self):
        return self.load_config(self.path)

    @staticmethod
    def load_config(path):
        
        with open(path, "r") as fp:
            config = json.load(fp)

        return config
