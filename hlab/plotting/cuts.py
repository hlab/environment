import matplotlib.pyplot as plt, numpy as np
from copy import deepcopy

from hlab.plotting.qc_plots import plot_1d

from qcodes.dataset.data_export import get_data_by_id, reshape_2D_data

def hcut( data_id, cut_value, plot_nr=0, do_plot=True, **kwargs ) :
    ''' Return a horizontal cut in qcodes format
    data_id: data id integer or a dataset
    plot_nr specifies which measured parameter to cut from. 
    e.g. if you measure two things in a do2d, plot_nr=1 is the second one
    do_plot: automatically plot the result
    **kwargs: passed on to plot_1d
    '''

    if isinstance(data_id, int):
        data=get_data_by_id( data_id )
    else:
        data=deepcopy( data_id )

    dicts =  data[ plot_nr ]
    x, y, z = reshape_2D_data( *map( lambda s : s['data' ], dicts ) )
    cut_idx = np.argmin( np.abs( y - cut_value ) )
    cut_value = y[ cut_idx ]
    dicts[0]['data'] = x
    dicts[2]['data'] = z[ cut_idx ]
    cut = dicts[0::2]

    if do_plot : plot_1d( cut, **kwargs )
    return cut

def vcut( data_id, cut_value, plot_nr=0, do_plot=True, **kwargs ) :
    ''' Return a vertical cut in qcodes format
    data_id: data id integer or a dataset
    plot_nr specifies which measured parameter to cut from. 
    e.g. if you measure two things in a do2d, plot_nr=1 is the second one
    kwargs gets passed on to plot_1d
    '''

    if isinstance(data_id, int):
        data=get_data_by_id( data_id )
    else:
        data=data_id

    dicts =  data[ plot_nr ]
    x, y, z = reshape_2D_data( *map( lambda s : s['data' ], dicts ) )
    cut_idx = np.argmin( np.abs( x - cut_value ) )
    cut_value = x[ cut_idx ]

    cut = [ dict(dicts[1]), dict(dicts[2]) ]
    cut[0]['data'] = y
    cut[1]['data'] = z[ :, cut_idx ]

    if do_plot : plot_1d( cut, **kwargs )
    return cut