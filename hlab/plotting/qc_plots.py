import matplotlib.pyplot as plt, numpy as np
import copy
from scipy.interpolate import griddata
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from qcodes import load_by_id
from qcodes.dataset.plotting import plot_by_id
from qcodes.dataset.plotting import plot_dataset, plot_on_a_plain_grid, _set_data_axes_labels, _rescale_ticks_and_units
from qcodes.dataset.data_export import reshape_2D_data

from hlab.data_io.dstruct import DataDict

def vave( datadict, do_plot=True ) :
    """ average datadict horizontally, along the x axis

    datadict: list of {data,unit,label} dicts
    
    Returns:
        a 1d datadict
    """
    
    averagedz = []
    xs = np.unique( datadict.x )
    for x in xs :
        argcut = datadict.x == x
        ave = np.mean( datadict.z[ argcut ] )
        averagedz.append( ave )
    

    xret = { 'data' : xs,
            'label' : datadict[0]['label'],
            'unit' :  datadict[0]['unit']}

    yret = { 'data' : np.array( averagedz ),
            'label' : datadict[2]['label'],
            'unit' : datadict[2]['unit'] }

    ave_dict = DataDict( (xret,yret) )
    if do_plot : plot_1d( ave_dict )
    return ave_dict
    
def have( datadict, do_plot=True ) :
    """ average datadict vertically, along the y axis

    datadict: list of {data,unit,label} dicts
    
    Returns:
        a 1d datadict
    """
    
    averagedz = []
    ys = np.unique( datadict.y )
    for y in ys :
        argcut = datadict.y == y
        ave = np.mean( datadict.z[ argcut ] )
        averagedz.append( ave )
    

    xret = { 'data' : ys,
            'label' : datadict[1]['label'],
            'unit' :  datadict[1]['unit']}

    yret = { 'data' : np.array( averagedz ),
            'label' : datadict[2]['label'],
            'unit' : datadict[2]['unit'] }

    ave_dict = DataDict( (xret,yret) )
    if do_plot : plot_1d( ave_dict )
    return ave_dict


def splot( *args, s : str='S_{11}', ax=None, legend=None ) :
    ''' 
    *args = n1, n2, n3 data id's
    plot mag/phase of s-parameter n with label s '''
    if ax is None: 
        fig, ax = plt.subplots( 2, 1, figsize=(12,8), sharex=True )


    for n in args :
        plot_by_id( n, ax )

    t = ax[0].get_title()
    exp_str = t.split(',')[1]
    ids = ' '.join( map( str, args ) )
    title = f'Run IDs: {ids},{exp_str}'
    for a in ax : a.set_title( title )

    if len( args ) > 1 : # set the legend
        for a in ax :
            a.legend( legend )

    fig.tight_layout()
    return fig, ax

def plot_reshaped( data, ax = None, cbar = None, 
                    rescale_axes=True, **kwargs ) :
    ''' Plot 2d data that has already been reshaped
    into 2d x, y, and z arrays

    returns: ax, cbar
    '''
    X, Y, z = map( lambda s : s['data'], data ) 
    if ax is None: 
        _, ax = plt.subplots( 1 )
    if cbar is None :
        pcm = ax.pcolormesh( X, Y, z, **kwargs )
        # cax = inset_axes(ax,
        #     width="100%",  # width = 50% of parent_bbox width
        #     height="3%",  # height : 5%
        #     loc='upper left',=
        #     bbox_to_anchor=(0, 0, 1, 1.05),
        #     bbox_transform = ax.transAxes,
        #     borderpad=0 )
        # cax = None
        cbar = ax.figure.colorbar( pcm, ax=ax, 
                    #   orientation='horizontal',
                    #   location='top',
                      aspect=100, pad=0.02 )
    else : # grab the limits
        vmin, vmax = ( cbar.vmin, cbar.vmax )
        if 'vmin' in kwargs : vmin = kwargs['vmin']
        if 'vmax' in kwargs : vmax = kwargs['vmax']

        pcm = ax.pcolormesh( X, Y, z, vmin=vmin, vmax=vmax, **kwargs )
        # cax = None

    _set_data_axes_labels(ax, data, cbar)
    clabel = '%s (%s)' % ( data[2]['label'], data[2]['unit'] )
    cbar.set_label( label=clabel, size='small' )
    if rescale_axes:
        _rescale_ticks_and_units(ax, data, cbar)

    plt.tight_layout()

    return ax, cbar

def plot_2d( data, ax = None, cbar = None, **kwargs ) :
    ''' Plot 2D data returned from qcodes get_data_by_id
    ax, cax : ( ax, cax ) : where to make the plot/colorbar
    cbar_loc: color bar location
    **kwargs: passed on to pcolormesh

    returns ax, cbar
    '''
    x, y, z = reshape_2D_data( *map( lambda s : s['data' ], data ) )
    X,Y = np.meshgrid( x, y )
    data_square = []
    for new, d in zip( ( X,Y,z), data ) :
        entry = dict( d )
        entry['data'] = new
        data_square.append( entry )
    return plot_reshaped( data_square, ax, cbar, **kwargs )

def plot_2d_mag( data, ax=None, **kwargs ) :
    ''' Plot magnitude of 2d data in dB
    return ax, cbar
    '''
    data_mag = data.copy()
    db = 20 * np.log10( np.abs( data_mag[2]['data'] ) )
    data_mag[2].update( {'label': '$|S_{21}|$', 'unit' : 'dB', 'data' : db } )
    return plot_2d( data_mag, ax=ax, **kwargs )

def plot_1d( data, ax=None, rescale_axes=True, sort_by_x=True, **kwargs ) :
    ''' Plot 1D data returned from qcodes get_data_by_id
    ax: plot on these axes
    rescale_axes: rescale units on axes? Set to False for log plots.
    sort_by_x: sort data by x-axis values?
    **kwargs are passed on to ax.plot()
    returns: ax
    '''
    if ax is None: _, ax = plt.subplots( 1 )

    x, y = tuple( map( lambda s : s['data'], data ) )
    if sort_by_x:
        order = x.argsort()
        x = x[order]
        y = y[order]
        if 'yerr' in kwargs :
            yerr = kwargs['yerr'] 
            if type( yerr ) is np.ndarray :
                kwargs['yerr'] = kwargs['yerr'][order]

    if 'label' not in kwargs :
        kwargs['label'] = data[1]['label']

    ax.errorbar( x, y, **kwargs )
    _set_data_axes_labels(ax, data)
    if rescale_axes:
        _rescale_ticks_and_units(ax, data)
    plt.tight_layout()
    return ax

def plot_2d_resampled( pid, nx=100, ny=100, plot_idx=0, **kwargs  ) :
    ''' Make a 2d plot of unevenly spaced points
    pid: dataset # to plot, or a loaded dataset
    plot_idx: plot # in the dataset
    nx: number of x points to sample
    ny: number of y points to sample
    kwargs: passed along to plot_reshaped
    returns reshaped and sampled data
    '''
    if isinstance(pid, int):
        data = DataDict.from_pid(pid)[ plot_idx ]
    else:
        data=pid
    
    x, y, z = data.xyz
    grid_x, grid_y = np.mgrid[ x.min():x.max():nx*1j, 
            y.min():y.max():ny*1j ]
    points = np.vstack((x,y)).T
    grid_z = griddata(points, z, (grid_x, grid_y), method='nearest')

    data_grid = DataDict( copy.deepcopy( data ) )
    data_grid.xyz = ( grid_x, grid_y, grid_z )

    ax, cbar = plot_reshaped( data_grid, **kwargs )
    if isinstance( pid, int ) :
        label = cbar.ax.get_xlabel()
        cbar.set_label( f'#{pid}, {label}' )
    return ax, cbar