import logging
import getpass
import os
from os.path import expanduser
import hlab
from pathlib import Path

def load_or_create_logger( logdir, experiment_name ):
    ''' Creates a logger in logdir with name experiment_name
    '''
    logger = logging.getLogger( experiment_name )
    
    #Make sure we don't accidentally add handlers to the logger.
    if not getattr(logger, 'handler_set', None):
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        log_file_name = experiment_name + '.log'

        path_to_log = os.path.join( logdir, log_file_name )
        logger.addHandler(get_file_handler(path_to_log, formatter))
        logger.handler_set = True

    adapter = CustomAdapter(logger, {'username' : getpass.getuser()})
    return  adapter


class CustomAdapter(logging.LoggerAdapter):
    def process(self, msg, kwargs):
        return '%s - %s' % (self.extra['username'], msg), kwargs

def get_file_handler(filepath, formatter, level=logging.DEBUG):
    #Make the file if it does not exist
    if not os.path.exists(filepath):
        with open(filepath, 'w'):pass
    
    h = logging.FileHandler(filepath)  
    h.setLevel(level)
    h.setFormatter(formatter)

    return h

    

def function_logger(project_name, path_to_log=None):
    logger = logging.getLogger(project_name)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    if not path_to_log:
        path_to_log = expanduser(os.path.join(hlab.config.current['scripts']['default_path'], "{}.log".format(project_name)))
    fh=logging.FileHandler(path_to_log)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    return logger
