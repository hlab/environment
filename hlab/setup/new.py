import hlab
import hlab.plotting.live
from pathlib import Path
import qcodes as qc
from os.path import expanduser
import os, sys
from shutil import copyfile
from hlab.log import hlog
import uuid
import subprocess

from plottr.apps import inspectr
from pyqtgraph.Qt import QtGui
from plottr import log as plottrlog

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

SETUP_STRING = """
hsetup = hlab.setup.new.experiment( sample='{}',
            path_to_station='{}',
            path_to_db='{}' )

hsetup.live_plot() # For live plotting with plottr
"""

def directory(**kwargs):
    ''' Sets up directories and .py files for a new experiment .
    Behavior can be configured by ~/.hlab/hlab.json
    '''
    dir = kwargs['dir']
    experiment_name = os.path.basename( dir )

    try:
        sample_name = kwargs['sample_name']
    except:
        sample_name = str(uuid.uuid4())


    script_file_name = "{}.py".format(experiment_name)
    template_path = os.path.join(SCRIPT_DIR, "template.py")
    script_folder_base = Path(hlab.config.current['scripts']['default_path']).expanduser()
    script_folder_path = script_folder_base.joinpath( dir )
    script_path = script_folder_path.joinpath(script_file_name)

    station_file_name = "station.yml"
    station_template_path = os.path.join(SCRIPT_DIR, station_file_name)
    station_path = os.path.join(script_folder_base, dir, station_file_name)

    if not os.path.exists(script_folder_path):
        Path(script_folder_path).mkdir(parents=True)
        msg = "Created script folder at: {}".format(script_folder_path)
    else:
        msg = "Using existing script folder at : {}".format(script_folder_path)
    print(msg)

    if not os.path.exists(script_path):
        copyfile(template_path, script_path)
        msg = "Created meaurement script : {} \n \
            from template: {}".format(script_path, template_path)
        print(msg)

        setup_string = SETUP_STRING.format( sample_name,
                    os.path.join( dir, station_file_name ),
                    os.path.join( dir, f'{experiment_name}.db' ) )

        with open(script_path, "a") as f:
            f.write(setup_string)

        #string from - (un)til, returns the strings s from the substring f until the substring t
        sf = lambda s, f, t : s[s.find(f)+len(f):s.find(t)]
        msg = "\nMeasurement script created with: {}".format(sf(setup_string, "import setup", "station, exp"))
        print(msg)
    else:
        msg = "Using existing measurement script : {}".format(script_path)
        print(msg)

    if not os.path.exists(station_path):
        copyfile(station_template_path, station_path)
        msg = "Created station file : {} \n \
            from template: {} ".format(station_path, station_template_path)
    else:
        msg = "Using existing station file : {}".format(station_path)
    print(msg)

import inspect 

def experiment_legacy( path_to_script, sample : str = None, path_to_station=None, path_to_db=None, 
                experiment : str = None, db_name=None ) :
    ''' Returns a set up HlabExp
    path_to_script is the path to the script creating this experiment
    sample: sample id string to be used by qcodes
    path_to_station: full path to .yml file. By default, will look in the same directory as path_to_script
    path_to_db: full path to qcodes .db file. By default, if measurement script is in $scripts/a/b/x.py will use $data/a/b/b.db
    experiment: qcodes experiment id. By default if the database is called b.db experiment name will be 'b'
    db_name: custom name for db. example db_name='poop' will be placed in $data/a/b/poop.db
    Sample is passed through to HlabExp
    '''

    script_dir = os.path.dirname( path_to_script )
    script_default = expanduser( hlab.config.current['scripts']['default_path'] )
    rel_script_dir = os.path.relpath( script_dir, script_default ) # path relative to ~/src
    db_default = expanduser( hlab.config.current['data']['default_path'] )

    sys.path.append( script_dir ) # so we can throw helper files into script dir

    if db_name is None :
        db_name = os.path.basename( rel_script_dir ) # default db name

    if path_to_station is None :
        path_to_station = os.path.join( script_dir, 'station.yml' )
    else :
        path_to_station = expanduser( path_to_station )

    if path_to_db is None :
        path_to_db = '%s/%s/%s.db' % ( db_default, rel_script_dir, db_name )
    else :
        path_to_db = expanduser( path_to_db )

    if experiment is None : # default to the name of the database
        experiment = os.path.basename( path_to_db ).split('.')[0]

    # return ( path_to_db, path_to_station, path_to_script, experiment, sample )
    hsetup = HlabExp( path_to_db, path_to_station, experiment, sample )

    return hsetup


def experiment( sample : str, 
                path_to_db : str, 
                path_to_station : str = '/', 
                experiment : str = None ) :
    ''' Returns a set up HlabExp. Sets hlab.setup.default

    Args:
        sample: sample id string to be used by qcodes
        path_to_station: path to .yml file. Will search relative to hlab.config.current['scripts']['default_path']
        path_to_db: path to qcodes .db file. Will search relative to hlab.config.current['data']['default_path']
        experiment: will guess based on path_to_db if no argument is supplied

    Sample is passed through to HlabExp
    '''

    script_default = expanduser( hlab.config.current['scripts']['default_path'] )
    db_default = expanduser( hlab.config.current['data']['default_path'] )

    if not os.path.exists( path_to_station ) :
        path_to_station = os.path.join( script_default, path_to_station )

    if not os.path.exists( path_to_db ):
        path_to_db = os.path.join( db_default, path_to_db )

    if experiment is None : # default to the name of the database
        experiment = os.path.basename( path_to_db ).split('.')[0]

    # return ( path_to_db, path_to_station, path_to_script, experiment, sample )
    hsetup = HlabExp( path_to_db, path_to_station, experiment, sample )
    hlab.setup.default = hsetup
    return hsetup


class HlabExp() :
    ''' Used for setting up an hlab experiment.
    scripts go in a folder named xx/project_name/experiment_name
    database then goes in db_root/project_name/experiment_name
    '''
    def __init__( self, path_to_db, path_to_station, experiment : str, sample : str ) :
        ''' Set up an hlab experiment with database located in db_root/experiment_name/
        '''
        self.path_to_db = path_to_db
        self.path_to_station = path_to_station
        self.experiment_name = experiment
        self.sample_name = sample

        # Setup DB and station
        # self.setupDB()
        isfile = os.path.isfile( self.path_to_station )

        if isfile :
            self.setupStation()
            self.with_station = True
        else :
            self.with_station = False

        self.setupDB()

        # setup qc folder-path, used in qdev_wrapper doNds to save pdfs. This
        qc.config.user.mainfolder = "{}".format( Path( self.db_dir ) )
        # qc.config.user.mainfolder = "{}".format(Path(self.db_root).joinpath(self.project_name))

        # Log the new experiment
        self.log = hlog.load_or_create_logger( self.db_dir, self.experiment_name )
        self.log.info("experiment name: {}".format( self.experiment_name ))
        self.log.info("sample name: {}".format(self.sample_name))
        self.log.info("db location: {}".format(Path(self.path_to_db).absolute()))
        
        if self.with_station:
            self.log.info("station file: {}".format(Path(self.path_to_station).absolute()))
        else:
            # raise FileNotFoundError('No station file!')
            self.log.warn("No station file")


        # Close file handlers
        for h in self.log.logger.handlers : h.close()

    @property
    def db_dir( self ) :
        return os.path.dirname( self.path_to_db )

    @property
    def db_name( self ) :
        ''' db_name without the .db '''
        file_name = os.path.basename( self.path_to_db )

        return file_name.split( '.db' )[0]

    @db_name.setter
    def db_name( self, new_name ) :
        new_path = f'{self.db_dir}/{new_name}.db'
        self.path_to_db = new_path
        self.experiment_name = new_name
        self.setupDB()
        
    def setupDB( self ) :
        ''' Set up database.
        sets it to self.db_dir
        '''
        p = Path(self.db_dir).expanduser()
        if not p.exists():
            p.mkdir(parents=True)
            msg = "Created db folder at: {}".format(self.db_dir)
            print(msg)
        qc.config.current_config['core']['path_to_db'] = self.path_to_db
        qc.config["core"]["db_location"] = self.path_to_db
        qc.initialise_database()
        self.exp = qc.load_or_create_experiment( self.experiment_name, sample_name=self.sample_name )

    def setupStation( self ) :
        ''' Set up a station 
        If there is already a qcodes default station, complain and exit.
        '''
        qc_default = qc.Station.default

        if qc_default is None:
            station = qc.Station(config_file=self.path_to_station)
            self.station = station
        else :
            raise FileExistsError( 'Station has already been created.' )


    def new_sample(self, sample_name) :
        self.sample_name = sample_name
        self.exp = qc.load_or_create_experiment( self.experiment_name, sample_name=self.sample_name )
        # Log and close file handlers
        self.log.info("changed sample name to: {}".format(self.sample_name))
        for h in self.log.logger.handlers : h.close()

    def live_plot( self ) :
        ''' Start live plotting as a subprocess.
        Stor subprocess output in self.plot_process 
        '''

        live = hlab.plotting.live.__file__
        p_out = subprocess.Popen( [ 'python', live, '--dbpath', self.path_to_db ] )
        self.plot_process = p_out
        # hlab.plotting.live.main( self.path_to_db )
