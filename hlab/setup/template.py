#%%
%matplotlib qt
import matplotlib.pyplot as plt, numpy as np, os
from qcodes import load_by_id, ScaledParameter
from qcodes.dataset.data_export import get_data_by_id
from  qcodes.dataset.plotting import plot_by_id
from hlab.data_io.qc_sweeps import do0d, do1d, do2d
import hlab, hlab.setup.new