#%%
import hlab
import os
from pathlib import Path
from hlab.utilities.dotdicts import dotdict


class PiMode():
    """Inspect the existing experiments in the datafolder
    defined in the hlab config
    """

    def __init__(self, root_dir=False):
        """Initialize by crawling though the datafolder and finding all
        dbs.

        Args:
            root_dir (bool, optional): Set a differeent data root directory
            if you don't want to use the one in hlab config. Defaults to False.
        """
        print("Loading all dbs, this can take a little bit")
        if not root_dir:
            self.root_dir = hlab.config.current['data']['default_path']
        else:
            self.root_dir = root_dir

        self.dbs = find_all_dbs(self.root_dir)


    def update(self, output=False):
        """Update the object with eventually new dbs

        Args:
            output (bool, optional): Return the new dbs. Defaults to False.

        Returns:
            diff (list[str,..]): new DB locations
        """
        new_dbs = find_all_dbs(self.root_dir)
        diff = set(new_dbs).difference(set(self.dbs))
        if diff:
            print("New dbs found!")
            for d in diff:
                print(d)
            self.dbs = new_dbs
        else:
            print("Nothing new")

        if output:
            return list(diff)



    def live_view_all(self, most_n_recent=False):
        """Start plottr live views for all dbs

        Args:
            most_n_recent (bool, optional): Just start the n most recently
            updated dbs. Defaults to False.
        """

        db_files = find_all_dbs(root_dir)

        if most_n_recent:
            for idx, f in enumerate(db_files):
                if idx<most_n_recent:
                    start(db_location=f)
                else:
                    break
        else:
            for f in db_files:
                start(db_location=f)

    @property
    def as_dict(self):
        """Browse dbs with dictionaries. Project names and experiment names
        are keys i.e. db_path = p.as_dict["project_name"]["experiment_name].
        Also supports dot notation i.e. p.project_name.experiment_name with
        autocomplete. In some cases the autocomplete does not work in the
        property, so you have to stick it into a variable to make it work.
        For example: d = p.as_dict, now, d. will autocomplete.

        Returns:
            dict of dicts: project - experiment dicts with the dbs.
        """
        dbs = dotdict()
        if self.dbs:
            for d in self.dbs:
                r = get_project_and_experiment(d)
                pr = dbs.setdefault(r['project'], dotdict())
                pr.setdefault(r['experiment'], d)
            return dbs
        else:
            print("No dbs yet")

    @property
    def projects(self):
        p = []
        for key, val in self.as_dict.items():
            print(key)
            p.append(key)
        return p

    @property
    def experiments(self):
        e = []
        for _, val in self.as_dict.items():
            for key, _ in val.items():
                print(key)
                e.append(key)
        return e





def agesorter(file):
    """Helper function used to order the dbs by the most recently updated

    Args:
        file (Path object): the file where we want to determine the age

    Returns:
        [float]: time when it was last updated
    """
    return Path(file).stat().st_mtime


def find_all_dbs(root_dir):
    """Crawl through root_dir and find all .db files and return
    their path objects, ordered with the most recent one first

    Args:
        root_dir ([str, or Path object]): root dir where to start searching
        for .db files

    Returns:
        list(Path obects,...): list of paths to dbs ordered by last
        update
    """
    db_files = db_files = [os.path.join(d, x)
            for d, dirs, files in os.walk(root_dir)
            for x in files if x.endswith(".db")]

    db_files.sort(key=agesorter, reverse=True)

    print("{} .db files found!".format(len(db_files)))

    return db_files

def get_project_and_experiment(path):
    p=Path(path).parts
    experiment_name = p[-2]
    project_name = p[-3]
    return {'project': p[-3], 'experiment': p[-2]}


