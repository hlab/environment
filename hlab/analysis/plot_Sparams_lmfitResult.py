# ducphys@gmail.com

from numpy import log10, abs, pi, angle, max, linspace, unwrap
from matplotlib import pyplot as plt
from matplotlib.ticker import FormatStrFormatter

def foo(arg = []):
    return '0'

def plot_SParams_vs_FitRes(freq, SComplx, lmfitResult, figsize = None, dpi = None, resolution = 1001):
    """
    Plot the complex S parameters together with the fit result evaluation
    in amplitude [dB] and phase [pi].
    freq: 1d array of frequency points
    SComplx: 1d array of complex S parameters
    lmfitResult: https://lmfit.github.io/lmfit-py/model.html#the-modelresult-class
    """
    
    if figsize is None:
        figsize = [8, 6]
    if dpi is None:
        dpi = 100
    
    fig, axarr = plt.subplots(2, 1, sharex = True, gridspec_kw={'height_ratios': [2, 1]},
                              figsize = figsize, dpi = dpi)
    for axi in axarr:
        axi.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        
    freqDense = linspace( freq[0], freq[-1], resolution, endpoint = True )
    # Plot amplitudes to axis 0
    axarr[0].plot( freq, 20 * log10( abs( SComplx ) ), '.', color = '#ec7014', )
    axarr[0].plot( freqDense, 20 * log10( abs( lmfitResult.eval( f = freqDense ) ) ), '-', color = '#238b45' )

    # Plot phase to axis 1
    axarr[1].plot( freq, unwrap(angle( SComplx )) / pi, '.', color = '#ec7014' )
    axarr[1].plot( freqDense, unwrap(angle( lmfitResult.eval( f = freqDense ) )) / pi, 
                    color = '#238b45')

    # Add axes' labels
    fntsze = 10
    axarr[0].set_ylabel("Amplitude [dB]", fontsize = fntsze)
    axarr[1].set_xlabel("Frequency [GHz]", fontsize = fntsze)
    axarr[1].set_ylabel("Phase [$\pi$ rad]", fontsize = fntsze)
    for tick in axarr[1].xaxis.get_major_ticks():
        tick.label.set_fontsize(fntsze) 
    for tick in axarr[1].yaxis.get_major_ticks():
        tick.label.set_fontsize(fntsze) 
    for tick in axarr[0].yaxis.get_major_ticks():
        tick.label.set_fontsize(fntsze) 
    
    # Set range + 10 %
    xr = 2/100.
    freqrange = freq.max() - freq.min()
    axarr[0].set_xlim(freq.min() - xr * freqrange, freq.max() + xr * freqrange)
    axarr[0].set_xlim(freq.min() - xr * freqrange, freq.max() + xr * freqrange)
        
    # Decoration
    axarr[0].grid(True)
    axarr[1].grid(True)
    
    # Add legends
    axarr[0].legend(["Data S11[dB]", "Fit S11[dB]"], loc = "lower left", fontsize = fntsze)
    axarr[1].legend(["Data arg(S11)[rad]", "Fit arg(S11)[rad]"], loc = "lower left", fontsize = fntsze)

    # Add text box showing fit result
    pars = lmfitResult.params
    statstr = '\n'.join((
        r'$f_0 = %.3f \pm %.1e [GHz]$' % (pars.get('f0').value, pars.get('f0').stderr, ),
        r'$\kappa_e = %.3f \pm %.1e [GHz]$' % (pars.get('kappaE').value, pars.get('kappaE').stderr, ),
        r'$\kappa_0 = %.3f \pm %.1e [GHz]$' % (pars.get('kappa0').value, pars.get('kappa0').stderr, ),
        r'$\phi_0 = %.3f \pm %.1e [rad]$' % (pars.get('phi0').value, pars.get('phi0').stderr, ),
        r'$\nu = %.3f \pm %.1e [rad/GHz]$' % (pars.get('nu').value, pars.get('nu').stderr,  )
        ))
    
    props = dict(boxstyle = 'round', facecolor = 'white', alpha = 0.4)
    axarr[0].text(1.01, 1.01, statstr, transform = axarr[0].transAxes, fontsize = fntsze, 
                  verticalalignment = 'top', horizontalalignment = 'right',
                  bbox = props)
    
    fig.tight_layout()

    return fig, axarr