''' Code for analyzing CBT data
Take a 2d bias/temperature sweep, and analyze using get_T()
'''
import numpy as np, scipy.constants as cst
import matplotlib.pyplot as plt

from qcodes.dataset.data_export import get_data_by_id, reshape_2D_data
from hlab.plotting.qc_plots import plot_1d, plot_2d
from lmfit.models import QuadraticModel, LinearModel

qmod = QuadraticModel()

def get_r( g2d ) :
    ''' Get ratio of dip.
    g2d: a name/unit/data/label 2d. 
        probably was returned by qcodes get_data_by_id
    returns: a name/unit/data/label 1d

    Iterate over the input 2d
    '''

    x, y, g = reshape_2D_data( *map( lambda s : s['data' ], g2d ) )
 
    # Analyze
    n1ds = g.shape[1]
    result = []
    for n in range( n1ds ) :
        cut = g[:,n]
        # trim NaN's from g and v
        not_nans = np.logical_not( np.isnan(cut) )
        cut = cut[ not_nans ]
        v = y[ not_nans ]

        r = get_gdg( v, cut, x[n] )
        result.append(r)

    # create a name/unit/data/label structure
    # Don't forget to set the units!
    result_dict = { 'name' : 'g_ratio', 
                'data' : np.array( result ), 
                'label' : '$g / \Delta g$', 
                'unit' : '' }

    return result_dict

def get_gdg( v, g, t ) :
    ''' Return g/dg
    v: voltage bias trace
    g: conductance trace
    t: temperature at which data are taken (used for parabola auto-range)
    '''
    # fit to parabola to get minimum
    qmin = quad_fit( v, g, t )

    trimmed = g[ np.abs( v ) > 3e-3 ]
    gmax = trimmed.mean()

    del_g = gmax-qmin
    r = gmax / del_g
    return r

def quad_fit( v, g, t ) :
    ''' Fit a parabola to the g vs v data at temperature t
    automatically guess the fit range based on the temperature

    rule of thumb for trim: 
    v: voltage bias trace
    g: conductance trace
    t: temperature at which data are taken (used for parabola auto-range)
    return: conductance minimum
    '''

    trim_frac = 50 # this times kT
    trim_range = max( 100e-6, trim_frac * cst.k * t / cst.e )
    v_in_range = np.abs( v ) < trim_range
    peak_trim = g[ v_in_range ]
    v_trim = v[ v_in_range ]

    qpar = qmod.guess(peak_trim, x= v_trim )
    out = qmod.fit(peak_trim, qpar, x=v_trim)

    b = out.params['b'].value
    a = out.params['a'].value
    vmin = - b/(2*a)
    qmin = out.eval( out.params, x=vmin )

    # # Plots for debugging fit
    # plt.figure()
    # plt.plot( v, g, marker='o', linestyle='' )
    # plt.plot( v_trim, out.best_fit )

    return qmin


# Extract the data
def analyze_temperature_2d( nplt, g2d_idx, t2d_idx, do_plot=True ) :
    ''' Analyze temperature/bias 2d plot

    nplt: plot number to analyzed
    do_plot: True
    returns analyzed data in name/unit/data/label format
    '''
    poop = get_data_by_id( nplt )
    g2d = poop[ g2d_idx ]
    t2d = poop[ t2d_idx ]
    *_, t = reshape_2D_data( *map( lambda s : s['data' ], t2d ) )
    t = t[0,:]
    t_dict = dict( t2d[2] ) # name/unit/data/label structure for plotting
    t_dict['data'] = t

    # analyze
    result_dict = get_r( g2d )
    formatted_output = [ t_dict, result_dict ]

    if do_plot : 
        plot_1d(  formatted_output, marker='o', linestyle='', markersize=3 )
    return formatted_output

def get_T(pid, fit_range = None, do_plot = True, g2d = 0, t2d = 1 ):
    '''
    Analyzer temperature 2D and fit line over input range
    Automatically trims NaN entries from analyzed data

    fit_range: temperature range to fit over
    g2d : conductance data index
    t2d : temperature data index

    Returns: temperature (Kelvin), line fit result
    '''

    if fit_range is None : fit_range = ( -np.infty, np.infty )
    
    # analyze data
    poop = analyze_temperature_2d( pid, g2d, t2d, do_plot=False )
     # Remove NaNs
    x, y = map( lambda s : s['data'], poop )
    notNans = np.logical_not( np.isnan( y ) )
    x = x[ notNans ]
    y = y[ notNans ]

    # Trim and fit
    yy = y[ ( x > fit_range[0] ) & ( x < fit_range[1] )]
    xx = x[ ( x > fit_range[0] ) & ( x < fit_range[1] )]
    model = LinearModel( nan_policy='omit' )
    params = model.guess(yy,x=xx)
    params['intercept'].value=0
    params['intercept'].vary=False
    result = model.fit(yy,params=params,x=xx )

    # Compute base temperature and Ecp
    ybase= y.min()
    tbase = get_x_from_y( ybase, result )
    ecp = get_Ecp( ybase, tbase )

    if do_plot : # plot results if requested
        plt_x = np.insert( x, 0, 0 )
        plot_1d( poop, linestyle='', marker='o', 
                markersize=3, color='black', label=f"#{pid}" )
        plt.plot( plt_x, result.eval( result.params, x=plt_x ), linewidth=1 )
        tstring = f"$T_\\mathrm{{base}}={tbase*1e3:.2f}$ mK"
        ecpstring = f"$E_\\mathrm{{cp}}={ecp*1e-9:.2f}$ GHz"
        plt.title( f"#{pid}: {tstring}, {ecpstring}")
        plt.tight_layout()
    return tbase, result

def get_x_from_y( y, result ) :
    ''' Return x-value associated with y-value for 
    linearfit result result
    '''
    # y = m * x + b
    m = result.params['slope']
    b = result.params['intercept']
    x = ( y - b ) / m
    return x

def get_Ecp( g_over_dg, T ) :
    ''' Calculate Ecp in Hz based on g/dg and temperature

    g/dg = 12 kT / Ecp

    return: Ecp in Hz
    '''
    Ecp_energy = 12 * cst.k * T / g_over_dg
    return Ecp_energy / cst.h
