# ducphys@gmail.com

import numpy as np

def S11ComplexFunction(f, kappa0, kappaE, f0, phi0, nu, scale):
    """
    Reflection amplitude for one-port model: Extrinsic coupling K_ex [GHz],
    intrinsic loss K_0 [GHz], resonance frequency f0 [GHz], independent variable 
    frequency f [GHz]
    Additional linear phase shift: constant phase shift phi0 [rad],
    linear relation nu [rad/GHz].
    scale is an overall attenuation factor
    """
    dKappa = 0.5 * (kappa0 - kappaE)
    sKappa = 0.5 * (kappa0 + kappaE)
    delta  = f - f0
    phaseshift = np.exp(-1j * (phi0 + nu * delta))
    return scale * phaseshift * (dKappa + 1j * delta) / (sKappa + 1j * delta)

def s11_phase( f, k, fr, m, b ) :
    ''' phase at frequency f for linewidth kappa and resonance fr
    assumes lossless resonator
    '''
    num = 4 * k * ( fr - f )
    denom = k**2 - 4 * ( fr - f )**2
    atan = np.arctan2( num, denom )
    return atan + m * f + b

if __name__ == "__main__":
    pass