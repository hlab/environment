# File name: S11OnePortModel.py
# Wrapper for lmfit model to fit kapp_e, kappa_i of one-port model resonance
# 25. Jun 2019
# ducphys@gmail.com
# NB: member function interactive() use nodejs and ipywidgets, see docstring.

import numpy as np
import matplotlib.pyplot as plt
import lmfit
from hlab.analysis.S11ComplexFunction import S11ComplexFunction
from hlab.analysis.plot_Sparams_lmfitResult import plot_SParams_vs_FitRes


class S11OnePortModelFitter:
    """Wrapping class for quick fitting complex S11 parameter with one-port model
    input-output theory.
    After initialization, all the fit parameters are set to some usual values.
    One can set different values and ranges with member functions:
     > setParameters( self, f0 = None, ke = None, k0 = None, nu = None, phi0 = None )
     > setParameterLimits( self, f0Lim = None, keLim = None, k0Lim = None, nuLim = None, phi0Lim = None )
    Then call the fit command with
     > fit()
    and plot with
     > plotDataFit()
    
    For the interactive plot, nodejs and npm packages have to be installed to the environment:
    (1) Install nodejs version 5+ from https://nodejs.org
    (2) In Anaconda promt:
        >> jupyter labextension install @jupyter-widgets/jupyterlab-manager
    """
    
    def __init__( self, freqArr = None, S11ComplxArr = None ) :
        '''
        Input parameters:
        freqArr: numpy array of float type
        S11ComplxArr: numpy array of complex type with identical length
        Initialize all parameters to usual values
        f0: Resonance frequency [GHz]
        ke: Extrinsic coupling [GHz]. Conventional ke is in [rad].
        k0: Intrinsic loss [GHz]
        nu: Linear phase shift factor [rad/GHz]
        phi0: Phase off-set [rad]
        '''
        self.freq = freqArr
        self.S11Complx = S11ComplxArr
        
        if self.freq is not None and self.S11Complx is not None:
            imin = np.argmin( np.abs( self.S11Complx ) )
            self.f0_guess =  self.freq[imin] #( min(self.freq) + max(self.freq) ) / 2 # [GHz]
            self.f0_limits = np.array( [min(self.freq), max(self.freq)], dtype = float )
        else:
            self.f0_guess = 5.0 # [GHz]
            self.f0_limits = np.array( [0.0, 0.0], dtype = float )
            
        self.ke_guess = 0.1 # [GHz]
        self.ke_limits = np.array( [0.0, 1.0] )
        
        self.k0_guess = 0.1 # [GHz]
        self.k0_limits = np.array( [0.0, 1.0] )
        
        self.nu_guess = 1.0 # [rad/GHz]
        self.nu_limits = np.array( [0.0, 10.0] )
        
        self.phi0_guess = 0.0 # [rad]
        self.phi0_limits = np.array( [-np.pi, np.pi] ) 

        self.scale_guess = 1.0 # Scaling factor
        self.scale_limits = np.array( [0.1, 2] )
                
        self.S11Model = lmfit.Model(S11ComplexFunction, independent_vars=['f'])
        self.fitmethod = 'leastsq'
        self.fitResult = None
        
        return None
    
    def setParameters( self, f0 = None, ke = None, k0 = None, nu = None, phi0 = None ):
        if f0 is not None:
            self.f0_guess = f0
        if ke is not None:
            self.ke_guess = ke
        if k0 is not None:
            self.k0_guess = k0
        if nu is not None:
            self.nu_guess = nu
        if phi0 is not None:
            self.phi0_guess = phi0
            
        return 0
    
    def setParameterLimits( self, f0Lim = None, keLim = None, k0Lim = None, nuLim = None, phi0Lim = None ):
        if f0Lim is not None:
            for idx in range(len(f0Lim)):
                self.f0_limits[idx] = f0Lim[idx]
        if keLim is not None:
            for idx in range(len(keLim)):
                self.ke_limits[idx] = keLim[idx]
        if k0Lim is not None:
            for idx in range(len(k0Lim)):
                self.k0_limits[idx] = k0Lim[idx]
        if nuLim is not None:
            for idx in range(len(nuLim)):
                self.nu_limits[idx] = nuLim[idx]
        if phi0Lim is not None:
            for idx in range(len(phi0Lim)):
                self.phi0_limits[idx] = phi0Lim[idx]
                
        return 0
    
    def makeParams( self ):
        self.S11Model.set_param_hint( 'kappa0', value = self.k0_guess, min = self.k0_limits[0], max = self.k0_limits[1] )
        self.S11Model.set_param_hint( 'kappaE', value = self.ke_guess, min = self.ke_limits[0], max = self.ke_limits[1] )
        self.S11Model.set_param_hint( 'f0', value = self.f0_guess, min = self.f0_limits[0], max = self.f0_limits[1] )
        self.S11Model.set_param_hint( 'phi0', value = self.phi0_guess, min = self.phi0_limits[0], max = self.phi0_limits[1] )
        self.S11Model.set_param_hint( 'nu', value = self.nu_guess, min = self.nu_limits[0], max = self.nu_limits[1] )
        self.S11Model.set_param_hint( 'scale', value = self.scale_guess, min = self.scale_limits[0], max = self.scale_limits[1] )
        
        pars = self.S11Model.make_params()
        
        return pars
    
    def fit( self ):
        self.makeParams()
        self.fitResult = self.S11Model.fit( self.S11Complx, f = self.freq, method = self.fitmethod )
        
        selfpars = self.fitResult.params
        self.fit_f0_stderr = np.array( [selfpars.get('f0').value, selfpars.get('f0').stderr] )
        self.fit_ke_stderr = np.array( [selfpars.get('kappaE').value, selfpars.get('kappaE').stderr] )
        self.fit_k0_stderr = np.array( [selfpars.get('kappa0').value, selfpars.get('kappa0').stderr] )
        self.fit_phi0_stderr = np.array( [selfpars.get('phi0').value, selfpars.get('phi0').stderr] )
        self.fit_nu_stderr = np.array( [selfpars.get('nu').value, selfpars.get('nu').stderr] )
        
        return self.fitResult
    
    def plotDataFit( self, figsize = None, dpi = 100, resolution = 1001 ):
        if self.fitResult is None:
            raise ValueError( "No fit results. Call fit() !" )
        
        return plot_SParams_vs_FitRes( self.freq, self.S11Complx, self.fitResult, \
                                       figsize = figsize, dpi = dpi , resolution = resolution)
    
    def plotDataModel( self, k0, ke, f0, phi0, nu, figsize = None, dpi = 100 , npts = 1000):
        """Plot magnitude and phase of S11 together with the S11ComplexFunction
        from the input output theory to compare the validity of input fit parameters.
        """
        if figsize is None:
            figsize = [8, 6]
            
        fig, ax = plt.subplots( 2, sharex = True, figsize = figsize, dpi = dpi )
        
        # Generate a denser array for plotting S11ComplexFunction()
        freqArr = np.arange( self.freq[0], self.freq[-1], \
                             np.abs( ( self.freq[0] - self.freq[-1] ) ) / npts )
        S11dB = np.array( [ 20 * np.log( np.abs( S11ComplexFunction( fi, k0, ke, f0, phi0, nu ) ) ) for fi in freqArr ] )
        argS11 = np.array( [ np.angle( S11ComplexFunction( fi, k0, ke, f0, phi0, nu ) ) for fi in freqArr ] )

        ax[0].plot( self.freq, 20 * np.log( np.abs(self.S11Complx) ), '.' ,color = 'red' )
        ax[1].plot( self.freq, np.angle( self.S11Complx ), '.', color = 'green' )

        ax[0].plot( freqArr, S11dB, color = 'black' )
        ax[1].plot( freqArr, argS11, color = 'grey' )

        ax[0].grid( True )
        ax[1].grid( True )

        ax[1].set_xlabel( "Frequency [GHz]" )
        ax[0].set_ylabel( "S11 [dB]" )
        ax[1].set_ylabel( "Phase [rad]" )

        ax[0].legend( ["Data S11 [dB]", "Model S11 [dB]"] )
        ax[1].legend( ["Data arg(S11) [rad]", "Model arg(S11) [rad]"] )
        fig.tight_layout()
            
        return 0
    
    def plotDataGuess( self, figsize = None, dpi = 100, npts = 1000 ):
        """Plot data and model with guessed fit parameters
        """
        self.plotDataModel( self.k0_guess, self.ke_guess, self.f0_guess, self.phi0_guess, self.nu_guess, \
                       figsize, dpi, npts )
        return 0
    
    def interactive( self, figsize = None, dpi = 100, npts = 1000, resolution = 1000 ):
        """Generate an interactive plot of data and model to find the guessed fit parameters manually.
        To use this, run command in the notebook
         >> %matplotlib inline
        """
        from ipywidgets import interact, fixed
        def interactPars( limits, resolution ):
            return ( limits[0], limits[1], abs( limits[0] - limits[1] ) / resolution )
        if figsize is None:
            figsize = [8, 6]
        interact( self.plotDataModel, \
                  k0 = interactPars( self.k0_limits, resolution ), \
                  ke = interactPars( self.ke_limits, resolution ), \
                  f0 = interactPars( self.f0_limits, resolution ), \
                  phi0 = interactPars( self.phi0_limits, resolution ), \
                  nu = interactPars( self.nu_limits, resolution ),\
                  figsize = fixed( figsize ), dpi = fixed( dpi ) , npts = fixed( npts ))
        
        return 0
    

if __name__ == "__main__":
    pass