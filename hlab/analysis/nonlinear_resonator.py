import numpy as np
import scipy.constants as cst 
from numpy.polynomial.polynomial import Polynomial
from scipy import interpolate
from lmfit import Model, Parameters
from hlab.data_io.dstruct import data1d_cmplx

def steady_state_pump_polynomial(delta, xi):
    """
    Steady state equation for intracavity pump photon number in a JPA.
    n = < aT * a >
    delta: pump detuning from resonance in line width unit
    xi: nonlinearity * pump power product in line width unit
    Roots can be found with Polynomial.roots().
    """
    c0 = -1.
    c1 = delta*delta + 1./4
    c2 = -2 * delta * xi
    c3 = xi * xi
    return Polynomial( [c0, c1, c2, c3] )

@np.vectorize
def get_n(delta, xi):
    rootns = steady_state_pump_polynomial(delta,xi).roots()
    keep = [x.imag == 0 for x in rootns]
    return rootns[keep][0].real

# TODO: add a hard record for interpolation function
npts = 200
dlow = -5
dup = 5
ds = np.linspace( dlow, dup, npts)
xilow = -1/3/np.sqrt(3)*0.999
xiup = -1e-5
xis = np.linspace( xilow, xiup, npts)
dds , xiss = np.meshgrid(ds, xis)
nss = get_n( dds, xiss )
fn = interpolate.interp2d(x=ds, y=xis, z=nss, kind='linear')

@np.vectorize
def ninterp(delta, xi):
    '''Interpolation function for reduced intracavity photon number'''
    return fn(delta,xi)[0]

def jparefl(delta, xi, kappa, gamma):
    '''Reflection coefficient from a nonlinear resonator'''
    n = ninterp(delta, xi)
    refl = kappa/(kappa+gamma) / (-1j*delta + 0.5 + 1j*xi*n) - 1
    return (refl.real - 1j*refl.imag)

def jparefl_scl( x, f0, xi, ktot, gkratio ):
    '''Reflection coefficient from a nonlinear resonator with 
    a scaled gamma-kappa-ratio as a parameter. 
    This is used to fix the loss-coupling ratio in fitting from the dip.
    '''
    delta = (x - f0)/ ktot
    n = ninterp( delta, xi )
    refl = 1./(1+gkratio) / (-1j*delta + 0.5 + 1j*xi*n) - 1.
    return (refl.real - 1j*refl.imag)

def jpa_phase( x, f0, xi, ktot, gkratio ):
    '''JPA phase response with corrected phase offset wrt normalized
    vna measured phase'''
    angle = np.angle( jparefl_scl( x, f0, xi, ktot, gkratio ) )
    return np.unwrap( angle - np.pi )

def jpa_amplitude( x, f0, xi, ktot, gkratio ):
    '''JPA amplitude response'''
    return np.abs( jparefl_scl( x, f0, xi, ktot, gkratio ) )

def fullrefl(x, A, v, phi0, f0, xi, kappa, gamma):
    '''Nonlinear resonator reflection coefficient with an environment
    factor.
    '''
    efactor = A * np.exp( -1j*(v*(x-f0) + phi0 - np.pi) )
    delta = (x - f0)/ (kappa+gamma)
    kern = jparefl(delta, xi, kappa, gamma)
    return efactor*kern

def jparefl_skew( x, f0, xi, ktot, gkratio, skew, phi0 ):
    delta = (x - f0)/ ktot
    n = ninterp( delta, xi )
    refl = 1./(1+gkratio*np.exp(1j*skew)) / (-1j*delta + 0.5 + 1j*xi*n) - 1
    refl *= np.exp( 1j*( phi0 - np.pi ) )
    return (refl.real - 1j*refl.imag)


def fit_s11_nlin( data : data1d_cmplx, paramsdict = None,
                method='least_squares', max_nfev = 1000):
    """Fit s11 from nonlinear resonator. Input has to be normalized so
    the environment factor is all 1. Frequency in GHz.
    paramsdict: { 'f0' : guessed_f0,
                    'xi': xi,
                    'ktot' : total_kappa,
                    'gkratio' : loss_externalkappa_ratio,
                    'skew' : skew_factor_for_impedance_mismatch,
                    'phi0' : phase_offset }
    """

    jpamod = Model(jparefl_skew) 
    pars = Parameters()
    guessed = dict()
    guessed.update({'f0' : data.x[np.argmin(np.abs(data))]})
    guessed.update({'xi' : 1e-3})
    guessed.update({'ktot' : 1e-2})
    guessed.update({'gkratio' : 0.5})
    guessed.update({'skew' : 0})
    guessed.update({'phi0' : 0})
    if paramsdict is not None:
        guessed.update(paramsdict)
    
    pars.add('f0', value=guessed['f0'], min=guessed['f0']*0.98, 
                    max=guessed['f0']*1.02 )
    pars.add('xi', value=guessed['xi'], min=-0.192, max=-1e-5)
    pars.add('ktot', value=guessed['ktot'], min=1e-3, max=guessed['ktot']*10 )
    pars.add('gkratio', value=guessed['gkratio'], min=0, max=2 )
    pars.add('phi0', value=guessed['phi0'], min=-np.pi, max=np.pi )
    pars.add('skew', value=guessed['skew'], min=-0.1, max=0.1 )

    fitres = jpamod.fit(data=data, params=pars, x=data.x, 
            method=method,
            max_nfev=max_nfev)

    return fitres
    
def fit_s11_nl_phase( data : data1d_cmplx, paramsdict = None,
                method='least_squares', max_nfev = 10000):
    """Fit s11 phase from nonlinear resonator. Input has to be normalized so
    the environment factor is all 1. Frequency in GHz.
    paramsdict: { 'f0' : guessed_f0,
                    'xi': xi,
                    'ktot' : total_kappa,
                    'gkratio' : loss_externalkappa_ratio}
    """

    jpamod = Model(jpa_phase) 
    pars = Parameters()
    guessed = dict()
    guessed.update({'f0' : data.x[np.argmin(np.abs(data))]})
    guessed.update({'xi' : 1e-3, 'xi_vary' : True})
    guessed.update({'ktot' : 1e-2})
    guessed.update({'gkratio' : 0.5})
    if paramsdict is not None:
        guessed.update(paramsdict)
    
    pars.add('f0', value=guessed['f0'], min=guessed['f0']*0.98, 
                    max=guessed['f0']*1.02 )
    pars.add('xi', value=guessed['xi'], min=-0.192, max=-1e-5,  
                    vary=guessed['xi_vary'] )
    pars.add('ktot', value=guessed['ktot'], min=1e-3, max=guessed['ktot']*10 )
    pars.add('gkratio', value=guessed['gkratio'], min=0, max=2 )

    fitres = jpamod.fit(data=np.unwrap(np.angle(data)), params=pars, x=data.x, 
            method=method,
            max_nfev=max_nfev)

    return fitres
    