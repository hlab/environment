import numpy as np
import matplotlib.pyplot as plt
from lmfit.model import Model
import lmfit
from scipy.interpolate import interp1d
from . import returnDelta, returnDelta_alpha

def returnDelta_GL(x):
    # x =T/Tc
    if  np.any(x)>1:
        return 0
    else:
        return np.sqrt(1-x)


# In[5]:


def freq_Tsweep_fit(T,Tc,c1,c2):
    delta = 1.764*Tc*returnDelta(T/Tc) # Delta0 = 1.76Tc
    x = c1*(1/(delta*np.tanh(delta/(2*T))))+c2
    return 1/(np.sqrt(x)) #Delta in K units


# In[6]:


def freq_Tsweep_fit_GL(T,Tc,c1,c2):
    delta = 1.766*Tc*returnDelta_GL(T/Tc) # Delta0 = 1.76Tc
    x = c1*(1/(delta*np.tanh(delta/(2*T))))+c2
    return 1/(np.sqrt(x)) #Delta in K units


# In[7]:


def freq_Tsweep_fit_alpha(T,Tc,c1,c2,Delta0):
    delta = Delta0*returnDelta_alpha(T/Tc) # Delta0 = Delta at alpha=zero
    x = c1*(1/(delta*np.tanh(delta/(2*T))))+c2
    return 1/(np.sqrt(x)) #Delta in K units


# In[8]:


class KineticInductanceModel(Model):
    __doc__ = """"f0 = 1 / sqrt((L_{kin}+L_{geo})*C)
    
    L_{kin}*C = constant_1 / (Delta * tanh(Delta / 2kB*T)) , where Delta depends on T_c
    
    c1 = geometric constants * R_(sq) * C
    and Delta is obtained from BCS theory
    
    L_{geo}*C = constant_2""" + lmfit.models.COMMON_DOC

    def __init__(self, *args, **kwargs):
        # constructor.
        super().__init__(freq_Tsweep_fit, *args, **kwargs)
    
    def modelfit(self,xdata,ydata,constant_1,T_c,constant_2=0,
                fixc1=False,fixTc=False,fixc2=False,do_plot=True):
        """
            Parameters:
        constant_1: formula defined above, includes geometrical factors and stray capacitance
        constant_2 (optional): geometrical inductance * stray capacitance (zero by default)
        T_c
        do_plot: plot if true

        Returns:
        Fit_parameters: Dictionary with parameter names as keys and best-fit values as values.   

        """
        super().set_param_hint('Tc', min=1e-3)
        super().set_param_hint('Tc', max=100)

        super().set_param_hint('c1', min=1e-10)
        super().set_param_hint('c1', max=1e4)

        super().set_param_hint('c2', min=0)

        super().set_param_hint('c1',vary = not fixc1)
        super().set_param_hint('Tc',vary = not fixTc)
        super().set_param_hint('c2',vary = not fixc2)
        
        params = super().make_params(Tc = T_c, c1=constant_1, c2=constant_2) 

        result = super().fit(ydata, params, T=xdata)

        if do_plot :
            print(result.fit_report())
            plt.plot(xdata, ydata, 'bo')
            plt.plot(xdata, result.best_fit, 'r-')
            plt.xlabel('Set T(K)')
            plt.ylabel('$f_{0}(GHz)$')
            plt.show()
            
        return result
    


# In[9]:


gmodel = Model(freq_Tsweep_fit)
# print('parameter names: {}'.format(gmodel.param_names))
# print('independent variables: {}'.format(gmodel.independent_vars))


# In[10]:


gmodel_GL = Model(freq_Tsweep_fit_GL)


# In[11]:


gmodel_alpha = Model(freq_Tsweep_fit_alpha)


# In[12]:


def fit_kineticinductance_model_GL(xdata,ydata,constant_1,T_c,constant_2=0):
    """This function fits the resonance frequency(xdata) vs temperature data(ydata) with the kinetic inductance model desribed below.
    
    f0 = 1 / sqrt((L_{kin}+L_{geo})*C)
    
    L_{kin}*C = constant_1 / (Delta * tanh(Delta / 2kB*T)) , where Delta depends on T_c
    
    and Delta is obtained from Ginzburg-Landau theory
    
    L_{geo}*C = constant_2

    Parameters:
    
        constant_1: formula defined above, includes geometrical factors and stray capacitance
        constant_2 (optional): geometrical inductance * stray capacitance (zero by default)
        T_c : critical temperature

    Returns:
        Fit_parameters: Dictionary with parameter names as keys and best-fit values as values.   

    """
    #yscale = abs(max(ydata)-min(ydata))
    
   # ydata = ydata/yscale #Normalizing data to avoid very large and small numbers
    #How to attain the fit paramters after scaling? (in general) 
    gmodel_GL.set_param_hint('Tc',min=1e-3)
    gmodel_GL.set_param_hint('Tc',max=100)
    
    gmodel_GL.set_param_hint('c1', min=1e-10)
    gmodel_GL.set_param_hint('c1', max=1e4)
    
    if constant_2 == 0:
        gmodel_GL.set_param_hint('c2',vary=False)
    else:
        gmodel_GL.set_param_hint('c2',min=0)
    
    #plt.plot(xdata,gmodel.eval(params,T=xdata))
    
    params = gmodel_GL.make_params(Tc = T_c, c1=constant_1, c2=constant_2) 
 
    result = gmodel_GL.fit(ydata, params, T=xdata)

    #ydata = ydata*yscale #Rescaling the data
    
    print(result.fit_report())
    plt.plot(xdata, ydata, 'bo')
    plt.plot(xdata, result.best_fit, 'r-')
    plt.xlabel('Set T(K)')
    plt.ylabel('$f_{0}(Hz)$')
    plt.show()
    return result.best_values


# In[13]:


def fit_kineticinductance_model(xdata,ydata,constant_1,T_c,constant_2=0,fixc1=False,fixTc=False,fixc2=False):
    """This function fits the resonance frequency(xdata) vs temperature data(ydata) with the kinetic inductance model desribed below.
    
    f0 = 1 / sqrt((L_{kin}+L_{geo})*C)
    
    L_{kin}*C = constant_1 / (Delta * tanh(Delta / 2kB*T)) , where Delta depends on T_c
    
    
    L_{geo}*C = constant_2

    Parameters:
        constant_1: formula defined above, includes geometrical factors and stray capacitance
        constant_2 (optional): geometrical inductance * stray capacitance (zero by default)
        T_c

    Returns:
        Fit_parameters: Dictionary with parameter names as keys and best-fit values as values.   

    """
#     yscale = abs(max(ydata)-min(ydata))
    
#   ydata = ydata/yscale #Normalizing data to avoid very large and small numbers
    #How to attain the fit paramters after scaling? (in general) 
    gmodel.set_param_hint('Tc',min=1e-3)
    gmodel.set_param_hint('Tc',max=100)
    
    gmodel.set_param_hint('c1', min=1e-10)
    gmodel.set_param_hint('c1', max=1e4)
    
    gmodel.set_param_hint('c2',min=0)
    
    if fixc1 == True:
        gmodel.set_param_hint('c1',vary = False)
    if fixTc == True :
        gmodel.set_param_hint('Tc',vary = False)
    if fixc2 == True:
        gmodel.set_param_hint('c2',vary = False)
        
    #plt.plot(xdata,gmodel.eval(params,T=xdata))
    
    params = gmodel.make_params(Tc = T_c, c1=constant_1, c2=constant_2) 
 
    result = gmodel.fit(ydata, params, T=xdata)

    #ydata = ydata*yscale #Rescaling the data
    
    print(result.fit_report())
    plt.plot(xdata, ydata, 'bo')
    plt.plot(xdata,result.best_fit, 'r-')
    plt.xlabel('Set T(K)')
    plt.ylabel('$f_{0}(Hz)$')
    plt.show()
    return result.best_values


# In[14]:


def fit_kineticinductance_model_alpha(xdata,ydata,constant_1,T_c,Delta0,constant_2=0):
    """This function fits the resonance frequency(xdata) vs temperature data(ydata) with the kinetic inductance model desribed below.
    
    f0 = 1 / sqrt((L_{kin}+L_{geo})*C)
    
    L_{kin}*C = constant_1 / (Delta * tanh(Delta / 2kB*T)) , where Delta depends on T_c
    
    
    L_{geo}*C = constant_2

    Parameters:
        constant_1: formula defined above, includes geometrical factors and stray capacitance
        constant_2 (optional): geometrical inductance * stray capacitance (zero by default)
        T_c

    Returns:
        Fit_parameters: Dictionary with parameter names as keys and best-fit values as values.   

    """
    yscale = abs(max(ydata)-min(ydata))
    
    ydata = ydata/yscale #Normalizing data to avoid very large and small numbers
    #How to attain the fit paramters after scaling? (in general) 
    gmodel_alpha.set_param_hint('Tc',min=1e-3)
    gmodel_alpha.set_param_hint('Tc',max=100)
    
    gmodel_alpha.set_param_hint('c1', min=1e-10)
    gmodel_alpha.set_param_hint('c1', max=1e4)
    
    if constant_2 == 0:
        gmodel_alpha.set_param_hint('c2',vary=False)
    else:
        gmodel_alpha.set_param_hint('c2',vary=True)
        gmodel_alpha.set_param_hint('c2',min=0)
    
    gmodel_alpha.set_param_hint('Delta0',vary=False)
    gmodel_alpha.set_param_hint('Tc',vary=False)
    gmodel_alpha.set_param_hint('c1',vary=False)
    params = gmodel_alpha.make_params(Tc = T_c, c1=constant_1,  c2=constant_2, Delta0= Delta0) 
    
    #plt.plot(xdata,yscale*gmodel_alpha.eval(params,T=xdata))
 
    result = gmodel_alpha.fit(ydata, params, T=xdata)

    ydata = ydata*yscale #Rescaling the data
    
    print(result.fit_report())
    plt.plot(xdata, ydata, 'bo')
    plt.plot(xdata, yscale*result.best_fit, 'r-')
    plt.xlabel('Set T(K)')
    plt.ylabel('$f_{0}(Hz)$')
    plt.show()
    return result.best_values


# In[ ]:




