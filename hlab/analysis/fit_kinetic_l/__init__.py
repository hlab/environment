import numpy as np, os
from scipy.interpolate import interp1d

# Set up interpolation files from .csv's

_interp_kwargs = { 'kind' : 'cubic', 
                    'fill_value' : (1.0,0.0),
                    'bounds_error' : False }


def _get_delta( s ) :
    ''' return normalized delta
    s: file to open, e.g. "gap_v_t.csv"
    '''
    __location__ = os.path.dirname( os.path.abspath( __file__ ) )
    fp = lambda s : os.path.join( __location__, s )
    gap_file = fp( "gap_v_t.csv" )
    return np.transpose(np.loadtxt( gap_file,delimiter=',') )


normalizedDelta  = _get_delta( "gap_v_t.csv" )
returnDelta = interp1d( *normalizedDelta, **_interp_kwargs )

normalized_Delta_alpha = _get_delta( "TvsDelta_alpha_0x44.csv" )
returnDelta_alpha = interp1d( *normalized_Delta_alpha, **_interp_kwargs )
