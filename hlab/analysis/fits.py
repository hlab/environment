from tkinter import N
from matplotlib import pyplot as plt
import numpy as np
from scipy.special import erf
from scipy.optimize import curve_fit
import scipy.constants as cst

from lmfit import Model
from lmfit.models import LinearModel, LorentzianModel, ConstantModel, ExponentialModel, ExpressionModel
from qcodes.dataset.data_export import get_data_by_id
from hlab.plotting.qc_plots import plot_1d
from hlab.data_io.dstruct import data1d_cmplx
from hlab.analysis.nonlinear_resonator import \
        ( jparefl_scl, jparefl_skew, jpa_phase, jpa_amplitude, fullrefl )

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'
    
def fit_line( data_n, plot_idx=0, r_cal=0, x_range=None, do_plot=True ) :
    ''' Fit a line in trace.
    
    Args:
        data_n: fit to #data_n if data_n is an integer.
        Otherwise assume its a dataset as returned by get_data_by_id, id (data1,data2,data3,...)
        plot_idx = 0 is the parameter number that was passed to do1d.
        r_cal = 0: calibration value to subtract off the fit, e.g. calibrated input impedance of i/v converter
        x_range: x_range=(x_start, x_end) x_range for fit. None means fir the whole data.
        attempt to parse units of the data and to label things in ohms
        otherwise just report the slope
    Returns:
        Model result, slope, axes of plot.
    '''
    if isinstance( data_n, int ):
        n=data_n
        data = get_data_by_id( n )[ plot_idx ]
    else:
        n="?"
        data = data_n[plot_idx]
    x, y = map( lambda s : s['data'], data )
    
    if x_range is not None: # trim data if requested
        x_range = np.array( x_range )
        x_range.sort()
        pts = ( x >= x_range[0] ) & ( x <= x_range[1] )
        y = y[ pts ]
        x = x[ pts ]

    x_unit, y_unit = map( lambda s : s['unit'], data )

    model = LinearModel( nan_policy='omit' )
    params = model.guess(y,x)
    result = model.fit(y,params=params,x=x )


    m = result.params['slope'].value
    if x_unit == 'A' and y_unit == 'V' : #V/A=Ohms
        r = m - r_cal
        label = f"R={r:.2e} $\\Omega$"
    elif x_unit == 'V' and y_unit == 'A' : #A/v = 1 / Ohms
        r = 1 / m - r_cal
        label = f"R={r:.2e} $\\Omega$"
    else :
        r = m
        label = f"m={m:.2e} {y_unit}/{x_unit}"

    if do_plot:
        fig, ax = plt.subplots(figsize=[4.2, 3.5])
        ax = plot_1d( data, ax=ax, marker='o', markersize=1, linestyle='' )
        ax.plot( data[0]['data'], result.eval( x=data[0]['data']), linewidth=1 )
        ax.set_title( f"#{n}: {label}" )
        ax.figure.tight_layout()
    else:
        ax = None
    
    return result, r

def fit_activation( data_n, plot_idx=0, x_idx=0, 
                x_range=None, do_plot=True ) :
    ''' Fit a exponential function in trace #data_n if data_n is an integer.
    Otherwise assume its a dataset as returned by get_data_by_id.

    plot_idx = 0 is the parameter number that was passed to do1d (as y)
    x_idx = 0 is another parameter that will be used as fit argument (as x)
    x_range: x_range=(x_start, x_end) x_range for fit. None means fir the whole data.
    '''
    if isinstance( data_n, int ):
        n=data_n
        data = get_data_by_id( n )[ plot_idx ]
        xdata = get_data_by_id( n )[ x_idx ]
    else:
        n="?"
        data = data_n[plot_idx]
        xdata = data_n[x_idx]
    x, y = map( lambda s : s['data'], data )
    if x_idx != plot_idx:
        _, x = map( lambda s : s['data'], xdata )

    if x_range is not None: # trim data if requested
        x_range = np.array( x_range )
        x_range.sort()
        pts = ( x >= x_range[0] ) & ( x <= x_range[1] )
        y = y[ pts ]
        x = x[ pts ]
    
    x_unit, y_unit = map( lambda s : s['unit'], data )
    if x_idx != plot_idx:
        _, x_unit = map( lambda s : s['unit'], xdata )
    
    model = ExponentialModel( nan_policy='omit' ) + ConstantModel()
    result = model.fit(y,x=x )

    def activation_model( x, A, beta, off ):
        return A*np.exp( beta / x ) + off

    model = Model( activation_model, nan_policy="omit" )
    params = model.make_params( A = max(y), beta = np.mean(x), off=min(x) )
    result = model.fit( data=y, params=params, x=x)

    b = result.params['beta'].value
    A = result.params['A'].value
    alabel = 'A = ' + f'{A :.2e} {y_unit} '
    if x_unit == 'K' : #V/A=Ohms
        label = r'$\mathrm{\beta}$ = ' + f'{b :.2f} K'
    else :
        label = r'$\mathrm{\beta}$ = ' + f'{x_unit}'
    label = alabel + label

    if do_plot:
        fig, ax = plt.subplots(figsize=[4.2, 3.5])
        if x_idx == plot_idx:
            ax = plot_1d( data, ax=ax, marker='o', markersize=1, linestyle='' )
        else:
            ax.plot( x, y, marker='o', markersize=1, ls='')
            ax.set_ylabel( data[1]['label'] + f'({y_unit})' )
            ax.set_xlabel( xdata[1]['label'] + f'({x_unit})')
        xgrid = np.linspace( min(x), max(x), 100 )
        ax.plot( xgrid, result.eval( x=xgrid, linewidth=1 ) )
        ax.set_title( f"#{n}: {label}" )
        ax.figure.tight_layout()
    return result

def fit_lorentzian( data_n, plot_idx=0, r_cal=0, x_range=None, y_range=None,
    do_plot=True, data_dB=None, eye_guess=None ) :
    ''' Fit a lorentzian in trace #data_n if data_n is an integer.
    Otherwise assume its a dataset as returned by get_data_by_id.
    eye_guess considers [fwhm, center, amplitude]
    plot_idx = 0 is the parameter number that was passed to do1d.
    x_range: x_range=(x_start, x_end) x_range for fit. None means fir the whole data.
    y_range:
    data_dB: Is data in DB? If none, auto-detect
    '''
    if isinstance( data_n, int ):
        n=data_n
        data = get_data_by_id( n )[ plot_idx ]
    else:
        n="?"
        data = data_n[plot_idx]
    x, y = map( lambda s : s['data'], data )
    
    if x_range is not None: # trim data if requested
        x_range.sort()
        xpts = ( x >= x_range[0] ) & ( x <= x_range[1] )
        y = y[ xpts ]
        x = x[ xpts ]

    if y_range is not None:
        ypts = ( y >= min(y_range) ) & ( y <= max(y_range) )
        y = y[ ypts ]
        x = x[ ypts ]

    if data_dB is None :
        if data[1]['unit'].lower() == 'db' :
            data_dB = True
        else :
            data_dB = False
    if data_dB:
        y = 10**(y/10)

    x_unit, y_unit = map( lambda s : s['unit'], data )


    cte_mod = ConstantModel()
    params = cte_mod.guess(y)

    lorentzian_mod = LorentzianModel()
    params.update(lorentzian_mod.guess(y,x))
    # params['sigma'].set(min=100e3)
    # params['amplitude'].set(max=0.1)

    if eye_guess is not None:
        params['fwhm'].value = eye_guess[0]
        params['sigma'].value = eye_guess[0]/2.
        params['center'].value = eye_guess[1]
        params['amplitude'].value = eye_guess[2]
        params['height'].value = eye_guess[2]/(eye_guess[0]/2.+np.pi)

    mod = lorentzian_mod + cte_mod
    init = mod.eval(params, x=x)
    result = mod.fit(y, params, x=x)
    label = f"None"
    fwhm = result.params['fwhm'].value
    center = result.params['center'].value
    if x_unit == 'V':
        fwhm = result.params['fwhm'].value
        label = f"FWHM={fwhm:.2e} V"
        sigma = result.params['sigma'].value
        label = f"Sigma={sigma:.2e} V"
    if y_unit == 'V' :
        height = result.params['amplitude'].value
        label = f"amplitude={height:.2e} V"
    else :
        label = ""
    ####
    if do_plot:
        qfactor = center/fwhm
        print('fc: '+str.format('{0:.3f}', center/1e9)+' GHz / fwhm: '+ str.format('{0:.3f}', fwhm/1e6)+ ' MHz / Q: '+str.format('{0:.4f}', qfactor) )
        ax = plot_1d( data, marker='o', markersize=1, linestyle='' )
        ax.plot( data[0]['data'], 10*np.log10(result.eval( x=data[0]['data'])) )
        ax.set_title( f"#{n}: {label} - Q: "+str.format('{0:.4f}', qfactor) )
        ax.figure.tight_layout()
    return result


def jq_noise( T, f, G, T_add ) :
    ''' Johnson noise + quantum noise + amplifier
    '''
    def coth( x ) : return 1 / np.tanh( x )
    zpf = cst.h * f / 2
    S = zpf * coth( zpf / ( cst.k * T ) )
    noise = G/(zpf) * ( S + cst.k * T_add )
    return noise

def resistance(I, V, dI, dV, show_plot = False, x_label = " ", y_label = " ", quiet=True):

    """Fit the resistance to an IV curve.

    Args:
        I (1D numpy array): The measured/set currents.
        V (1D numpy array): The measured/set voltages.
        dI (float): factor to convert the I datapoints to Ampere
        dV (float): factor to convert the V datapoints to Volt
        show_plot (optional, bool): Flag whether to plot the fitted line over the data
        x_label (optional, str) : X label for the IV plot
        y_label (optional, str) : Y label for the IV plot
        quiet (optional, bool) : Supress output if true

    Returns:
        fit_V (1D numpy array) : Fitted V values corresponing to the measured I values
        resistance (float): Fitted resistance in Ohm.

    """

    def line(x,a,b):
        return (a*x) + b

    params, std = curve_fit(line ,I ,V )

    fit_V = line(I, *params)

    # 'a' in line corresponds to the resistance (V/I). Multiply with the prefractos to get
    # resistance in SI units.
    resistance = params[0] * (dV / dI)


    print(color.BOLD + color.RED + "Resistance: " + str(resistance) + " Ohm" + color.END)

    if not quiet :
        print("Resistance: " + str(resistance) + " Ohm")

    if show_plot:
        plt.plot(I,V)
        plt.plot(I, fit_V)
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.show()

    return fit_V, resistance, params[1]


def DualLinear(x, k0, y0, xcross, sigma, k1, y1):
    """One line (k1,y1) overtaking another line (k0,y0) at xcross"""
    sigma = sigma # 1e-3
    def erfsigma(x, _pars1):
        """_pars2[0] = x0
        _pars2[1] = sigma"""
        return erf( (x-_pars1[0])/_pars1[1] )
    overpass = erfsigma( x, [xcross, sigma] )
    return 0.5*(1-overpass)*( k0*x+y0 ) + 0.5*(1+overpass)*( k1*x+y1 )

def fit_crosslines( data_n, plot_idx=0, r_cal=0, x_range=None, do_plot=True ) :
    ''' Fit two crossed lines in trace #data_n if data_n is an integer.
    Otherwise assume its a dataset as returned by get_data_by_id.

    plot_idx = 0 is the parameter number that was passed to do1d.
    r_cal = 0: calibration value to subtract off the fit, e.g. calibrated input impedance of i/v converter
    x_range: x_range=(x_start, x_end) x_range for fit. None means fir the whole data.
    attempt to parse units of the data and to label things in ohms
    otherwise just report the slope
    '''
    if isinstance( data_n, int ):
        n=data_n
        data = get_data_by_id( n )[ plot_idx ]
    else:
        n="?"
        data = data_n[plot_idx]
    x, y = map( lambda s : s['data'], data )

    if x_range is not None: # trim data if requested
        x_range = np.array( x_range )
        x_range.sort()
        pts = ( x >= x_range[0] ) & ( x <= x_range[1] )
        y = y[ pts ]
        x = x[ pts ]

    x_unit, y_unit = map( lambda s : s['unit'], data )

    model = Model(DualLinear)
    result = model.fit(data=y,x=x,k0=5e-1,y0=-1e-2,xcross=20e-3, sigma = 1e-3, k1=1e-3,y1=-1e-3)

    m = result.params['k0'].value
    if x_unit == 'A' and y_unit == 'V' : #V/A=Ohms
        r = m - r_cal
        label = f"R={r:.2e} $\\Omega$"
    elif x_unit == 'V' and y_unit == 'A' : #A/v = 1 / Ohms
        r = 1 / m - r_cal
        label = f"R={r:.2e} $\\Omega$"
    else :
        r = m
        label = f"m={m:.2e} {y_unit}/{x_unit}"

    if do_plot:
        ax = plot_1d( data, marker='o', markersize=1, linestyle='' )
        ax.plot( data[0]['data'], result.eval( x=data[0]['data']), linewidth=1 )
        ax.set_title( f"#{n}: {label}" )
        ax.figure.tight_layout()
    return result, r
