import sys
from importlib.machinery import PathFinder, ModuleSpec, SourceFileLoader
import imp

# Following: https://blog.sqreen.com/dynamic-instrumentation-agent-for-python/ 


class CustomFileLoader():

    def __init__(self, loader, fullname, finder):
        self.loader = loader
        self.fullname = fullname
        self.finder = finder

    def exec_module(self, module):
        if hasattr(self.loader, "exec_module"):
            self.loader.exec_module(module)
        else:
            module = self.loader.load_module(self.fullname)

        return self.finder._patch(module, self.fullname)

    def create_module(self, spec):
        return None


class CustomModuleFinder():
    def __init__(self):
        self.patcher_functions = {}

        self.pathfinder = PathFinder()

    def register_before(self, module_name, object_name, patcher_function):
        self._register("pre_execution", module_name, object_name, patcher_function)

    def register_after(self, module_name, object_name, patcher_function):
        self._register("post_execution", module_name, object_name, patcher_function)

    def _register(self, lifecycle, module_name, object_name, patcher_function):
        module_patchers = self.patcher_functions.setdefault(module_name, {})
        object_patchers = module_patchers.setdefault(
            object_name, {"pre_execution": [], "post_execution": []}
        )
        object_patchers[lifecycle].append(patcher_function)

    def add_patch(self, module_name, class_name, function, after=False):

        if not after:
            self.register_before(module_name, class_name, function)
        else:
            self.register_after(module_name, class_name, function)


    def start(self):
        if self not in sys.meta_path:
            sys.meta_path.insert(0, self)
        print("patched!")

    def find_spec(self, fullname, path=None, target=None):
        if fullname not in self.patcher_functions:
            return

        from importlib.machinery import ModuleSpec

        spec = self.pathfinder.find_spec(fullname, path, target)

        if not spec:
            return

        return ModuleSpec(fullname, CustomFileLoader(spec.loader, fullname, self))

    def _get_module(self, fullname):
        splitted_name = fullname.split(".")
        parent = ".".join(splitted_name[:-1])

        if fullname in sys.modules:
            return sys.modules[fullname]

        elif parent in sys.modules:
            parent = sys.modules[parent]
            module_path = imp.find_module(splitted_name[-1], parent.__path__)
            return imp.load_module(fullname, *module_path)

        else:
            try:
                module_path = imp.find_module(fullname)
                return imp.load_module(fullname, *module_path)

            except ImportError:
                print("Import error...")

    def _patch(self, module, fullname):
        objects_to_patch = self.patcher_functions.get(fullname, {})

        for object_name, patcher_callbacks in objects_to_patch.items():
            object_path = object_name.split(".")

            original = self._get_object(module, object_path)

            if original is None:
                continue

            patched_function = new_function(original, **patcher_callbacks)
            self._set_object(module, object_path, patched_function)

        return module

    def _get_object(self, module, object_path):
        current_object = module

        for part in object_path:
            try:
                current_object = getattr(current_object, part)
            except AttributeError:
                return None

        return current_object

    def _set_object(self, module, object_path, new_object):
        object_to_patch = self._get_object(module, object_path[:-1])
        setattr(object_to_patch, object_path[-1], new_object)



def new_function(original, pre_execution=None, post_execution=None):
    
    
    if not pre_execution:
        pre_execution = []
    if not post_execution:
        post_execution = []


    def wrapper(*args, **kwargs):

        for function in pre_execution:
            try:
                function(original,*args, **kwargs)
            except:
                print("Callback failed; function: " + function.__name__)
        
        return_value = original(*args, **kwargs)
        
        for function in post_execution:
            try:
                function(original, return_value, *args, **kwargs)
            except:
                print("Callback failed; function: " + function.__name__)

        return return_value
    return wrapper