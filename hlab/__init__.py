from hlab.log.hlog import function_logger
from hlab.utilities.monkey import CustomModuleFinder
from hlab.config import Config
import matplotlib.pyplot as plt


#setup or find the config file.
config = Config()

# Default matplotlib font sizes
plt.rcParams.update({'font.size': 14,
                    'axes.titlesize' : 'small',
                    'figure.titlesize' : 'small' })
# 
# l = function_logger()

# def log_this(original, *args, **kwargs):
#     l.info("{} called with {} , {}".format(original.__name__, args, kwargs))

# mf = CustomModuleFinder()

# mf.patch('qcodes.dataset.experiment_container','load_or_create_experiment',log_this)
# mf.patch('qcodes.dataset.sqlite.database', 'initialise_database', log_this)
# mf.start()


