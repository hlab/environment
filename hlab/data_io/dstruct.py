#### Data structures used for handling qcodes data ####
# DataDict adds some functionality on top of the structure returned by get_data_by_id
# data1d and data1d_cmplx are for legacy purposes and should probably not be used any more.

import numpy as np
import matplotlib.pyplot as plt
import json
from scipy.optimize import curve_fit
from qcodes.dataset.data_export import load_by_id, _get_data_from_ds, get_data_by_id
from scipy.interpolate import interp1d
from copy import deepcopy

class DataDict() :
    
    def __init__( self, dicts, tag=None ) :
        ''' DataDict class. wraps the data structure returned by get_data_by_id.
        Raw data structure is stored in self.data.

        Args:
            dicts: iterable of data structures, as returned by get_data_by_id
            tag: tag to store in self.tag. often pid/database associated with dataset
        '''
        for d in dicts :
            d['data'] = np.array( d['data'] ).flatten()
            
        self.data = dicts
        self.tag = tag

    @classmethod
    def from_pid( cls, pid ) :
        ''' load data using get_data_by_id( pid )[idx]
        return a data_dict constructed from this

        Will add dict.pid to every dataset loaded
        '''
        dataset = load_by_id( pid )
        data = _get_data_from_ds(dataset)
        dicts = []
        for d in data : 
            dicts.append( cls( d, tag=pid ) )
        return dicts

    @classmethod
    def concatenate( cls, dicts ) :
        """ Concatenate several DataDicts. Assumes all dictionaries have same dimensions/units.
        Args:
            dicts: (dict1, dict2, dict3, ...), some iterable of DataDicts.
        Returns:
            A concatenated datadict
        """

        def cat_ax( ax ) :
            """ concatenate a single axis 
            Args:
                ax: # of axis to concatenate
            """
            values = [ d[ax]['data'] for d in dicts  ]
            return np.concatenate( values )

        xyz = [ cat_ax( ax ) for ax in range( len( dicts[0] ) ) ]
        dest = dicts[0].empty_dict()
        dest.xyz = xyz
        return dest

    def empty_dict( self ) :
        """ Make a dictionary with empty data but with this dictionary's unit and labels.
        Returns:
            DataDict with empty data
        """
        new = []
        for ax in self :
            new.append( dict( label=ax['label'], unit=ax['unit'], data=np.array([]) ) )
        return DataDict( new )


    def __getitem__( self, i ) :
        ''' subscripts give self.data
        '''
        return self.data[i]

    def __setitem__( self, i, newvalue ) :
        ''' make subscripts setable
        '''
        self.data[i] = newvalue

    def __len__( self ) :
        return len( self.data )

    def __repr__( self ) :
        return self[-1]['label']

    @property
    def x( self ) :
        ''' return x as np array
        '''
        return self._get_data( 0 )
    @x.setter
    def x( self, data ) :
        self._set_data( 0, data )

    @property
    def y( self ) :
        ''' return y as np array
        '''
        return self._get_data( 1 )
    @y.setter
    def y( self, data ) :
        self._set_data( 1, data )

    @property
    def z( self ) :
        ''' return z as np array
        '''
        return self._get_data( 2 )
    @z.setter
    def z( self, data ) :
        self._set_data( 2, data )

    def _get_data( self, n ) :
        ''' return nth dataset
        '''
        return self[n]['data']

    def _set_data( self, n, data ) :
        ''' set nth dataset to data
        '''
        self[n]['data'] = data

    @property
    def xyz( self ) :
        ''' x, y, z as np arrays (or however many elements there are).
        will return however many entries are in the measurement 
        ( e.g. len( self ) )

        returns a map that gives x, y, z, ... data
        '''
        return map( lambda s : s['data'], self )

    @xyz.setter
    def xyz( self, xyznew ) :
        ''' set the xyz data
        '''
        for dnew, dold in zip( xyznew, self ) :
            dold['data'] = dnew 

    def export( self, filename ) :
        """ 
        Export as json.
        Args:
            filename: filename as a string
        """
        with open( filename, 'w') as f:
            json.dump( self._tolist(), f)

    def cut_at_x( self, value, tol=None ) :
        """ Take a cut at x=value
        Args:
            value: value to take cut at
            tol: tolerance for cut. keep if this close
        Returns:
            cut as DataDict
        """
        if tol is None :
            keep = self.x == value
        else :
            keep = np.abs( self.x - value ) < tol

        cut = DataDict.empty_dict((self[1],self[2]))
        cut.x = self.y[ keep ]
        cut.y = self.z[ keep ]
        cut.tag = self.tag
        return cut

    def cut_at_y( self, value, tol=None ) :
        """ Take a cut at y=value
        Args:
            value: value to take cut at
            tol: tolerance for cut. None means require exact match
        Returns:
            cut as DataDict
        """
        if tol is None :
            keep = self.y == value
        else :
            keep = np.abs( self.y - value ) < tol
            
        cut = DataDict.empty_dict((self[0],self[2]))
        cut.x = self.x[ keep ]
        cut.y = self.z[ keep ]
        cut.tag = self.tag
        return cut

    def _tolist( self ) :
        """ return data in listified form, ready for json export
            Args: data as datadict
        
            Returns: listified data
        """
        def entry_to_list( o ) :
            """ convert np array to list if needed """
            if type( o ) is np.ndarray :
                return o.tolist()
            else :
                return o

        def dict_list( d ) :
            """ convert all entries in dict """
            return { k : entry_to_list( val ) for k, val in d.items() }

        return [ dict_list( d ) for d in self.data ]

def fast_reshape( x, y, z ) :
    ''' do a fast reshape of 2d data

    data should be x, y, z 1ds as generated by qcodes
    makes lots of assumptions about dataset being nicely square
    returns x, y, z reshaped and plottable by pcolormesh
    '''

    xlong, ylong, zlong = x, y, z
    for i, yz in enumerate( ylong[1:] - ylong[:-1] ) :
        if yz < 0 :
            ny = i+1
            break
    nx = int( len( ylong ) / ny )

    x = np.reshape( xlong, (nx, ny ) )
    y = np.reshape( ylong, (nx, ny ) )
    z = np.reshape( zlong, (nx, ny ) )

    return x, y, z


class data1d( np.ndarray ) :

    ''' Class for holding 1d data trace.
    Supplies some functionality for slicing, keeping x units, and labeling

    x/y are data traces
    x/ylabel labels the x and y axes
    This is called before init.

    '''

    def __new__( cls, y, ylabel, x=None, xlabel=None, run_id=None ) :
        ''' Lets you return the thing that will get passed to __init__(self)
        '''
        obj = np.asarray( y ).view( cls ) #view cast 
        obj.ylabel = ylabel
        if x is None :
            obj.x = np.arange( obj.shape[0] )
        else :
            obj.x = np.asarray( x )
        obj.xlabel = xlabel
        obj.run_id = run_id

        return obj

    def __init__( self, y, ylabel, x=None, xlabel=None, run_id=None ) :
        '''
        Everything is done in __new__, this is just for the docstring the class signature
        '''
        pass

    def __array_finalize__(self, obj):
        '''This gets called is somebody viewcasts or instantiates by a template.
        '''
        if obj is None: return
        self.ylabel = getattr( obj, 'ylabel', None )
        self.xlabel = getattr( obj, 'xlabel', None )
        self.x = getattr( obj, 'x', None )
        self.run_id = getattr( obj, 'run_id', None )
        
        try: # This is for handling slices, see __getitem__
            self.x = self.x[ obj._new_startidx ]
        except:
            pass

    def __getitem__(self, item):
        ''' Handle slices for x automatically '''
        try:
            if isinstance(item, (slice, int)):
                self._new_startidx = item
            else:
                self._new_startidx = item[0]
        except: 
            pass
        return super().__getitem__(item)

    def __array_ufunc__( self, ufunc, method, *inputs, **kwargs ) :
        ''' Handle numpy ufuncs. Copy over class metadata and viewcast to ndarray.
        
        - *ufunc* is the ufunc object that was called.
        - *method* is a string indicating how the Ufunc was called, either
        ``"__call__"`` to indicate it was called directly, or one of its
        :ref:`methods<ufuncs.methods>`: ``"reduce"``, ``"accumulate"``,
        ``"reduceat"``, ``"outer"``, or ``"at"``.
        - *inputs* is a tuple of the input arguments to the ``ufunc``
        - *kwargs* contains any optional or keyword arguments passed to the
        function. This includes any ``out`` arguments, which are always
        contained in a tuple.
        '''
        args = []
        in_no = []
        for i, input_ in enumerate(inputs): # viewcast args if appropriate
            if isinstance( input_, data1d ):
                in_no.append(i)
                args.append(input_.view( np.ndarray ) )
            else:
                args.append(input_)
        

        outputs = kwargs.pop('out', None) # given output arrays
        out_no = []
        if outputs:
            out_args = []
            for j, output in enumerate(outputs):
                if isinstance( output, data1d ):
                    out_no.append(j)
                    out_args.append(output.view(np.ndarray))
                else:
                    out_args.append(output)
            kwargs['out'] = tuple(out_args)
        else:
            outputs = (None,) * ufunc.nout

        info = {}
        if in_no:
            info['inputs'] = in_no
        if out_no:
            info['outputs'] = out_no

        results = super( data1d, self).__array_ufunc__(ufunc, method, 
                                    *args, **kwargs)
        if results is NotImplemented:
            return NotImplemented

        if method == 'at':
            # if isinstance(inputs[0], data1d): # Assign metadata
            #     inputs[0].info = info
            return

        if ufunc.nout == 1:
            results = (results,)

        results = tuple( (np.asarray(result).view( data1d ) 
                        if output is None else output) 
                        for result, output in zip(results, outputs) )
        if results and isinstance( results[0], data1d ):
            if in_no != [] : #copy metadata over from first data1d input
                in_1d = inputs[ in_no[ 0 ] ]
                results[0].__dict__.update( in_1d.__dict__)

        return results[0] if len(results) == 1 else results

    def x2p( self, x0 ) :
        ''' Convert x to a point number
        '''
        diff = np.abs( self.x - x0 )
        return np.argmin( diff )

    def p2x( self, p ) :
        ''' Convert point number to an x value
        '''
        return self.x[ p ]

    def __call__( self, x1, x2=None ) :
        ''' Return the value at x=x1, or the data from x1 to x2 '''
        p1 = self.x2p( x1 )
        if x2 is None :
            return self[ p1 ]
        else :
            p2 = self.x2p( x2 )
            return self[ p1 : p2 ]


    def plot( self, ax=None, **kwargs ) :
        ''' Plot the data and label them axes!
        subplot is the axis to plot on
        Return the axis we plotted on
        '''
        if ax is None :
            _, ax = plt.subplots( 1, 1 )

        ax.plot( self.x, self, **kwargs )
        ax.set_xlabel( self.xlabel )
        ax.set_ylabel( self.ylabel )
        
        if self.run_id is not None :
            ax.set_title( 'Run_id #%d' % self.run_id )
        plt.tight_layout()
        return ax

    def apply( self, f ) :
        ''' 
        return a data1d where y = f( self ), x = self.x
        Note that numpy ufuncs are natively supported
        e.g. out = data.apply( np.unwrap )
        '''
        out = f( self ).view( data1d )
        out.__dict__.update( self.__dict__ )
        return out

    def centered( self, cen_, range_ ):
        """Extension of __call__ to get a centered slice of data."""
        obj = self.__call__( cen_ - range_, cen_ + range_)
        return obj

    def fit( self, f, guess : tuple, do_fit=True, do_plot=True ) :
        '''
        Utility function for fitting
        Fit self to f using scipy curve fit.
        Plot the results. If do_fit=False just plot the guess
        '''

        if do_fit is False :
            popt, pcov = guess, None
        else :
            popt, pcov = curve_fit( f, self.x, self, p0=guess )

        if do_plot :
            ax = self.plot()
            ax.plot( self.x, f( self.x, *popt ), linestyle='dashed' )
            plt.tight_layout()
        else :
            ax = None
            
        return popt, pcov, ax

class data1d_cmplx( data1d ) :
    ''' Subclass data1d to handle complex numbers
    '''
    def __new__( cls, mag_db, phase_rad, ylabel, x=None, xlabel=None, run_id=None ) :
        ''' Lets you return the thing that will get passed to __init__(self)
        '''
        mag = np.power( 10, mag_db/20 )
        obj = np.asarray( mag * np.exp( 1.j * phase_rad ) ).view( cls ) #view cast 
        obj.ylabel = ylabel
        obj.x = np.asarray( x )
        obj.xlabel = xlabel
        obj.run_id = run_id

        return obj

    def __init__( self, mag_db, phase_rad, ylabel, x=None, xlabel=None, run_id=None ) :
        '''
        Everything is done in __new__, this is just for the docstring the class signature
        '''
        pass

    def __array_finalize__(self, obj):
        '''This gets called is somebody viewcasts or instantiates by a template.
        '''
        if obj is None: return
        self.ylabel = getattr( obj, 'ylabel', None )
        self.xlabel = getattr( obj, 'xlabel', None )
        self.x = getattr( obj, 'x', None )
        self.run_id = getattr( obj, 'run_id', None )
        
        try: # This is for handling slices, see __getitem__
            self.x = self.x[ obj._new_startidx ]
        except:
            pass
        
    def apply( self, f ) :
        ''' 
        return a data1d where y = f( self ), x = self.x
        e.g. out = data.apply( np.unwrap )
        '''
        out = f( self ).view( data1d_cmplx )
        out.__dict__.update( self.__dict__ )
        return out

    def __truediv__( self, divisor ):
        if isinstance(divisor, data1d_cmplx):
            xmin = max(min(self.x), min(divisor.x))
            xmax = min(max(self.x), max(divisor.x))
            f = lambda x : interp1d( x=divisor.x, y= divisor.data )(x)
            trimmed = self(xmin, xmax)
            new_y = np.true_divide(trimmed, f(trimmed.x) ).view(data1d_cmplx)
            return new_y
        elif isinstance(divisor, (int, float, complex)):
            return self/divisor

    @classmethod
    def from_cmplx( cls, cmplx, ylabel, x, xlabel, run_id=None ) :
        ''' Create a data1d_cmplx from complex data
        '''
        mag_db = 20 * np.log10( np.abs( cmplx ) )
        phase = np.angle( cmplx )
        return cls( mag_db, phase, ylabel, x, xlabel, run_id )

    @classmethod
    def from_pid( cls, pid, ylabel='S_{21}' ) :
        ''' Create a data1d_cmplx from qcodes plot id
        assumes the plot took magnitude and phase in that order
        '''

        data = get_data_by_id( pid )
        mag, phase = map( lambda s : s[1]['data'], data )
        freq = data[0][0]['data']

        return cls( mag, phase * np.pi / 180, ylabel, x=freq, xlabel='$f$ (GHz)', run_id=pid )

    def plot( self, ax=None, **kwargs ) :
        ''' Make a subplot of magnitude and phase 
        return plot axes
        '''
        if ax is None :
            fig, ax = plt.subplots( 2, 1, figsize = [6.2,4.8], sharex = True )
        else:
            try:
                fig = ax.get_figure()
            except:
                fig = ax[0].get_figure()

        # Plot magnitude
        fontsize = 10
        mag_db = 20 * np.log10( np.abs( self ) )
        ax[0].plot( self.x, mag_db, **kwargs )
        ax[0].set_xlabel( None )
        label = "$|%s|$ (dB)" % self.ylabel
        ax[0].set_ylabel( label, fontsize=fontsize )
        if self.run_id is not None:
            fig.suptitle( f"Run_id #{self.run_id}", fontsize=fontsize )
            #case: fig not created

        # Plot phase
        phase = np.unwrap( np.angle( self ) )
        ax[1].plot( self.x, phase, **kwargs )
        ax[1].set_xlabel( self.xlabel, fontsize=fontsize )
        label = "$\\angle %s$ (rad)" % self.ylabel
        ax[1].set_ylabel( label, fontsize=fontsize )
        plt.tight_layout( rect=[0, 0.03, 1, 0.95] )
        return ax

def unwrap_data1d_cmplx( cmplx_data: data1d_cmplx, phase_offset: float =0 ) -> data1d_cmplx:
    """Unwrap the phase of 1d complex data and add a 
    phase_offset in rad
    """
    
    # get phase and amplitude
    phase = np.angle(cmplx_data) #[rad]
    amp = np.abs(cmplx_data)
    
    # unwrap phase part
    phase_unwrap = np.unwrap(phase) - phase_offset
    
    # reconstruct data1d_cmplx object
    raw_unwrap =  amp * np.exp( 1.j * phase_unwrap )
    unwrap_data = data1d_cmplx.from_cmplx( raw_unwrap, ylabel='S_{21}', 
                        x=cmplx_data.x, xlabel=cmplx_data.xlabel )

    return unwrap_data