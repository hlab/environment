import numpy as np


def read_qcodes_dat(path, starting_line=3):
    """Import data from Georgios heliox / custom qcodes i.e. the .dat
    files. They present the data in x y format after a header

    Args:
        path (str): Path to the measured data
        starting_line (int): Line from which the data starts after the header

    Returns:
        x (np.array)  : First column in .dat file
        y (np.array)  : Second column in .dat file

    """
    with open(path, "r") as read_file:
        data_lines = read_file.readlines()

    x = []
    y = []

    for line in data_lines[starting_line:]:
        try:
            x.append(float(line.split()[0]))
            y.append(float(line.split()[1]))
        except:
            pass

    return np.array(x),np.array(y)