from contextlib import contextmanager
from typing import Callable, Sequence, Union, Tuple, List, Optional, Iterator
import os
import time

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

from qcodes.dataset.measurements import Measurement, res_type, DataSaver
from qcodes.instrument.specialized_parameters import ElapsedTimeParameter
from qcodes.instrument.base import _BaseParameter
from qcodes.dataset.plotting import plot_by_id
from qcodes import config

ActionsT = Sequence[Callable[[], None]]

ParamMeasT = Union[_BaseParameter, Callable[[], None]]

AxesTuple = Tuple[matplotlib.axes.Axes, matplotlib.colorbar.Colorbar]
AxesTupleList = Tuple[List[matplotlib.axes.Axes],
                      List[Optional[matplotlib.colorbar.Colorbar]]]
AxesTupleListWithRunId = Tuple[int, List[matplotlib.axes.Axes],
                      List[Optional[matplotlib.colorbar.Colorbar]]]


def _process_params_meas(param_meas: ParamMeasT) -> List[res_type]:
    output = []
    for parameter in param_meas:
        if isinstance(parameter, _BaseParameter):
            output.append((parameter, parameter.get()))
            # if isinstance( parameter, ParameterWithSetpoints ) : # get setpoints
            #     for s in parameter.setpoints :
            #         output.append((s, s()))
        elif callable(parameter):
            parameter()
    return output


def _register_parameters(
        meas: Measurement,
        param_meas: List[ParamMeasT],
        setpoints: Optional[List[_BaseParameter]] = None
) -> None:
    for parameter in param_meas:
        if isinstance(parameter, _BaseParameter):
            meas.register_parameter(parameter,
                                    setpoints=setpoints)


def _register_actions(
        meas: Measurement,
        enter_actions: ActionsT,
        exit_actions: ActionsT
) -> None:
    for action in enter_actions:
        # this omits the possibility of passing
        # argument to enter and exit actions.
        # Do we want that?
        meas.add_before_run(action, ())
    for action in exit_actions:
        meas.add_after_run(action, ())



def _set_write_period(
        meas: Measurement,
        write_period: Optional[float] = None
) -> None:
    if write_period is not None:
        meas.write_period = write_period


@contextmanager
def _catch_keyboard_interrupts() -> Iterator[Callable[[], bool]]:
    interrupted = False
    def has_been_interrupted():
        nonlocal interrupted
        return interrupted
    try:
        yield has_been_interrupted
    except KeyboardInterrupt:
        interrupted = True


def do0d(
    *param_meas:  ParamMeasT,
    write_period: Optional[float] = None,
    before_meas_actions: ActionsT = (),
    do_plot: bool = True
) -> AxesTupleListWithRunId:
    """
    Perform a measurement of a single parameter. This is probably most
    useful for an ArrayParamter that already returns an array of data points

    Args:
        *param_meas: Parameter(s) to measure at each step or functions that
          will be called at each step. The function should take no arguments.
          The parameters and functions are called in the order they are
          supplied.
        do_plot: should png and pdf versions of the images be saved after the
            run.

    Returns:
        The run_id of the DataSet created
    """
    meas = Measurement()
    _register_parameters(meas, param_meas)
    _set_write_period(meas, write_period)

    with _catch_keyboard_interrupts() as interrupted, meas.run() as datasaver:
        for action in before_meas_actions: 
            action()
        datasaver.add_result(*_process_params_meas(param_meas))

    return _handle_plotting(datasaver, do_plot, interrupted())

def do1d_arb(
    param_set: _BaseParameter, 
    iter : Iterator, delay: float,
    *param_meas: ParamMeasT,
    enter_actions: ActionsT = (),
    exit_actions: ActionsT = (),
    before_meas_actions: ActionsT = (),
    write_period: Optional[float] = None,
    do_plot: bool = True
) -> AxesTupleListWithRunId:
    """
    Perform a 1D scan of ``param_set`` over ``iter``
    measuring param_meas at each step. In case param_meas is
    an ArrayParameter this is effectively a 2d scan.

    Args:
        param_set: The QCoDeS parameter to sweep over
        iter: iterable containing x values
        delay: Delay after setting paramter before measurement is performed
        *param_meas: Parameter(s) to measure at each step or functions that
          will be called at each step. The function should take no arguments.
          The parameters and functions are called in the order they are
          supplied.
        enter_actions: A list of functions taking no arguments that will be
            called before the measurements start
        exit_actions: A list of functions taking no arguments that will be
            called after the measurements ends
        before_meas_actions: A list of function taking no argument that will 
            be called before adding results to the datasaver
        do_plot: should png and pdf versions of the images be saved after the
            run.

    Returns:
        The run_id of the DataSet created
    """
    meas = Measurement()
    _register_parameters(meas, (param_set,))
    _register_parameters(meas, param_meas, setpoints=(param_set,))
    _set_write_period(meas, write_period)
    _register_actions(meas, enter_actions, exit_actions)
    param_set.post_delay = delay

    # do1D enforces a simple relationship between measured parameters
    # and set parameters. For anything more complicated this should be
    # reimplemented from scratch
    with _catch_keyboard_interrupts() as interrupted, meas.run() as datasaver:
        for set_point in iter:
            param_set.set(set_point)
            for action in before_meas_actions: 
                action()
            datasaver.add_result((param_set, set_point),
                                  *_process_params_meas(param_meas))
    return _handle_plotting(datasaver, do_plot, interrupted())

def do2d_arb( param1 : _BaseParameter, iter1 : Iterator, delay1 : float, 
        param2 : _BaseParameter, iter2 : Iterator, delay2 : float, 
        *params : ParamMeasT, 
        set_before_sweep: Optional[bool] = True,
        enter_actions: ActionsT = (),
        exit_actions: ActionsT = (),
        before_inner_actions: ActionsT = (),
        after_inner_actions: ActionsT = (),
        before_meas_actions: ActionsT = ()
) -> None :
    '''
    Perform a 2d scan with arbitrary point spacing on both axes.
    TODO: use the fancy built-in functions to register parameters, take data. Specifically:
        with _catch_keyboard_interrupts() as interrupted, meas.run() as datasaver:
    
    Args: 
        Measure 1d trace versus time
        time_pts: number of points to take
        time_delay: delay between time points
        other arguments the same as do1d

    Measure *params vs time, with npts and an extra delay
    '''

    meas = Measurement()
    meas.register_parameter(param1)
    meas.register_parameter(param2)
    _register_parameters(meas, (param1, param2) )
    _register_parameters(meas, params, setpoints=(param1, param2))
    _register_actions(meas, enter_actions, exit_actions)

    for measure in params:
        meas.register_parameter( measure, setpoints=(param1, param2) )

    with meas.run() as datasaver:

        for val1 in iter1:
            if set_before_sweep:
                param2.set(iter2[0])
            param1(val1)
            time.sleep(delay1)
            for action in before_inner_actions:
                    action()
            for val2 in iter2:
                param2(val2)
                time.sleep( delay2 )
                for action in before_meas_actions: 
                    action()
                datasaver.add_result((param1, val1),
                                         (param2, val2),
                                         *_process_params_meas( params ) )

            for action in after_inner_actions:
                action()


def do1d(
    param_set: _BaseParameter, start: float, stop: float,
    num_points: int, delay: float,
    *param_meas: ParamMeasT,
    enter_actions: ActionsT = (),
    exit_actions: ActionsT = (),
    before_meas_actions: ActionsT = (),
    write_period: Optional[float] = None,
    do_plot: bool = True
) -> AxesTupleListWithRunId:
    """
    Perform a 1D scan of ``param_set`` from ``start`` to ``stop`` in
    ``num_points`` measuring param_meas at each step. In case param_meas is
    an ArrayParameter this is effectively a 2d scan.

    Args:
        param_set: The QCoDeS parameter to sweep over
        start: Starting point of sweep
        stop: End point of sweep
        num_points: Number of points in sweep
        delay: Delay after setting paramter before measurement is performed
        *param_meas: Parameter(s) to measure at each step or functions that
          will be called at each step. The function should take no arguments.
          The parameters and functions are called in the order they are
          supplied.
        enter_actions: A list of functions taking no arguments that will be
            called before the measurements start
        exit_actions: A list of functions taking no arguments that will be
            called after the measurements ends
        before_meas_actions: A list of function taking no argument that will 
            be called before adding results to the datasaver
        do_plot: should png and pdf versions of the images be saved after the
            run.

    Returns:
        The run_id of the DataSet created
    """
    meas = Measurement()
    _register_parameters(meas, (param_set,))
    _register_parameters(meas, param_meas, setpoints=(param_set,))
    _set_write_period(meas, write_period)
    _register_actions(meas, enter_actions, exit_actions)
    param_set.post_delay = delay

    # do1D enforces a simple relationship between measured parameters
    # and set parameters. For anything more complicated this should be
    # reimplemented from scratch
    with _catch_keyboard_interrupts() as interrupted, meas.run() as datasaver:
        for set_point in np.linspace(start, stop, num_points):
            param_set.set(set_point)
            for action in before_meas_actions: 
                action()
            datasaver.add_result((param_set, set_point),
                                  *_process_params_meas(param_meas))
    return _handle_plotting(datasaver, do_plot, interrupted())


def do2d(
    param_set1: _BaseParameter, start1: float, stop1: float,
    num_points1: int, delay1: float,
    param_set2: _BaseParameter, start2: float, stop2: float,
    num_points2: int, delay2: float,
    *param_meas: ParamMeasT,
    set_before_sweep: Optional[bool] = False,
    enter_actions: ActionsT = (),
    exit_actions: ActionsT = (),
    before_inner_actions: ActionsT = (),
    after_inner_actions: ActionsT = (),
    before_meas_actions: ActionsT = (),
    write_period: Optional[float] = None,
    flush_columns: bool = False,
    do_plot: bool=True
) -> AxesTupleListWithRunId:

    """
    Perform a 1D scan of ``param_set1`` from ``start1`` to ``stop1`` in
    ``num_points1`` and ``param_set2`` from ``start2`` to ``stop2`` in
    ``num_points2`` measuring param_meas at each step.

    Args:
        param_set1: The QCoDeS parameter to sweep over in the outer loop
        start1: Starting point of sweep in outer loop
        stop1: End point of sweep in the outer loop
        num_points1: Number of points to measure in the outer loop
        delay1: Delay after setting parameter in the outer loop
        param_set2: The QCoDeS parameter to sweep over in the inner loop
        start2: Starting point of sweep in inner loop
        stop2: End point of sweep in the inner loop
        num_points2: Number of points to measure in the inner loop
        delay2: Delay after setting paramter before measurement is performed
        *param_meas: Parameter(s) to measure at each step or functions that
          will be called at each step. The function should take no arguments.
          The parameters and functions are called in the order they are
          supplied.
        set_before_sweep: if True the inner parameter is set to its first value
            before the outer parameter is swept to its next value.
        enter_actions: A list of functions taking no arguments that will be
            called before the measurements start
        exit_actions: A list of functions taking no arguments that will be
            called after the measurements ends
        before_inner_actions: Actions executed before each run of the inner loop
        after_inner_actions: Actions executed after each run of the inner loop
        before_meas_actions: A list of function taking no argument that will 
            be called before adding results to the datasaver
        do_plot: should png and pdf versions of the images be saved after the
            run.

    Returns:
        The run_id of the DataSet created
    """

    meas = Measurement()
    _register_parameters(meas, (param_set1, param_set2))
    _register_parameters(meas, param_meas, setpoints=(param_set1, param_set2))
    _set_write_period(meas, write_period)
    _register_actions(meas, enter_actions, exit_actions)

    param_set1.post_delay = delay1
    param_set2.post_delay = delay2

    with _catch_keyboard_interrupts() as interrupted, meas.run() as datasaver:
        for set_point1 in np.linspace(start1, stop1, num_points1):
                if set_before_sweep:
                    param_set2.set(start2)

                param_set1.set(set_point1)
                for action in before_inner_actions:
                    action()
                for set_point2 in np.linspace(start2, stop2, num_points2):
                    # skip first inner set point if `set_before_sweep`
                    if set_point2 == start2 and set_before_sweep:
                        pass
                    else:
                        param_set2.set(set_point2)
                    for action in before_meas_actions: 
                        action()
                    datasaver.add_result((param_set1, set_point1),
                                         (param_set2, set_point2),
                                         *_process_params_meas(param_meas))
                for action in after_inner_actions:
                    action()
                if flush_columns:
                    datasaver.flush_data_to_database()

    return _handle_plotting(datasaver, do_plot, interrupted())




def _handle_plotting(
        datasaver: DataSaver,
        do_plot: bool = True,
        interrupted: bool = False
) -> AxesTupleList:
    """
    Save the plots created by datasaver as pdf and png

    Args:
        datasaver: a measurement datasaver that contains a dataset to be saved
            as plot.
            :param do_plot:

    """
    dataid = datasaver.run_id
    if do_plot == True:
        res = _create_plots(datasaver)
    else:
        res = dataid, None, None

    if interrupted:
        raise KeyboardInterrupt

    return res


def _create_plots(datasaver: DataSaver) -> AxesTupleList:
    dataid = datasaver.run_id
    plt.ioff()
    start = time.time()
    axes, cbs = plot_by_id(dataid)
    stop = time.time()
    print(f"plot by id took {stop - start}")
    mainfolder = config.user.mainfolder
    experiment_name = datasaver._dataset.exp_name
    sample_name = datasaver._dataset.sample_name
    storage_dir = os.path.join(mainfolder, experiment_name, sample_name)
    os.makedirs(storage_dir, exist_ok=True)
    png_dir = os.path.join(storage_dir, 'png')
    pdf_dif = os.path.join(storage_dir, 'pdf')
    os.makedirs(png_dir, exist_ok=True)
    os.makedirs(pdf_dif, exist_ok=True)
    save_pdf = True
    save_png = True
    for i, ax in enumerate(axes):
        if save_pdf:
            full_path = os.path.join(pdf_dif, f'{dataid}_{i}.pdf')
            ax.figure.savefig(full_path, dpi=500)
        if save_png:
            full_path = os.path.join(png_dir, f'{dataid}_{i}.png')
            ax.figure.savefig(full_path, dpi=500)
    plt.ion()
    res = dataid, axes, cbs
    return res

## NEEDS TO BE MADE FANCY STILL with _handle_plotting and the like ##
def dovt( npts : int, delay : float, *params : ParamMeasT,
    before_meas_actions: ActionsT = () ) -> None :
    ''' Measure *params vs time, with npts and an extra delay
    '''
    t = ElapsedTimeParameter('Time')
    meas = Measurement()
    meas.register_parameter(t)
    for p in params :
        meas.register_parameter( p, setpoints=[t])

    with meas.run() as datasaver:
        output = []
        t.reset_clock()
        for _ in range( npts ):
            time.sleep( delay )    
            for action in before_meas_actions: 
                action()
            now = t()
            datasaver.add_result( (t, now), *_process_params_meas(params) )
    return datasaver.run_id
    # _save_image( datasaver )


def dovt_outer( time_pts, time_delay, set_param, 
    start, stop, npts, delay, *params,
    before_inner_actions: ActionsT = (),
    before_meas_actions: ActionsT = () ) :
    ''' 
    Measure 1d trace versus time
    time_pts: number of points to take
    time_delay: delay between time points
    other arguments the same as do1d
    '''
    t = ElapsedTimeParameter('Time')
    meas = Measurement()
    meas.register_parameter(t)
    meas.register_parameter(set_param)
    for p in params :
        meas.register_parameter( p, setpoints=(t,set_param) )

    with meas.run() as datasaver:
        t.reset_clock()
        for _ in range( time_pts ):
            time.sleep( time_delay )
            now = t()

            for action in before_inner_actions :
                action()
            t.reset_clock()

            for set_v in np.linspace( start, stop, npts ) :
                set_param( set_v )
                time.sleep( delay )
                
                for action in before_meas_actions: 
                    action()
                output = []
                for p in params :
                    output.append( ( p, p.get() ) )
                datasaver.add_result( (t, now), (set_param, set_v), *output )
    # _save_image( datasaver )

def dovt_inner( outer_param, outer_iter, outer_delay, 
    npts : int, delay : float, *params,
    before_inner_actions: ActionsT = (),
    before_meas_actions: ActionsT = () 
    ) :
    ''' sweep outer parameter, and taking a time trace at each value
    outer_param: outer paramater to set
    outer_iter: iterable for outer parameter
    outer_delay: delay for outer parameter
    npts: number of points for time sweep
    delay: delay for time sweep
    *params: parameters to measure
    '''
    t = ElapsedTimeParameter('Time')
    meas = Measurement()
    meas.register_parameter(t)
    meas.register_parameter(outer_param)
    for p in params :
        meas.register_parameter( p, setpoints=(t,outer_param) )

    with meas.run() as datasaver:
        output = []
        for outer_val in outer_iter :
            outer_param( outer_val )
            time.sleep( outer_delay )

            for action in before_inner_actions :
                action()
            t.reset_clock()
            for _ in range( npts ):
                time.sleep( delay )
                now = t()
                for action in before_meas_actions: 
                    action()
                for p in params :
                    output.append( ( p, p.get() ) )
                datasaver.add_result( (outer_param, outer_val), 
                            (t, now), 
                            *output )


def do2d_arbmesh(
    param_set1: _BaseParameter,
    param_set2: _BaseParameter,
    mesh: 'iter_of_(set1, set2)',
    delay1: float,
    delay2: float,
    *param_meas: ParamMeasT,
    set_before_sweep: Optional[bool] = False,
    enter_actions: ActionsT = (),
    exit_actions: ActionsT = (),
    before_inner_actions: ActionsT = (),
    after_inner_actions: ActionsT = (),
    before_meas_actions: ActionsT = (),
    write_period: Optional[float] = None,
    flush_columns: bool = False,
    do_plot: bool=True
) -> AxesTupleListWithRunId:

    """
    Perform a linearized scan of ``param_set1`` and ``param_set2`` 
    with respective setpoints (set1, set2) from mesh[0] to mesh[-1] 
    and measuring param_meas at each step.

    Args:
        param_set1: The QCoDeS parameter to sweep over in the outer loop
        param_set2: The QCoDeS parameter to sweep over in the inner loop
        mesh: a list of 1d-array with respective setpoint values (set1, set2)
        delay1: delay after setting the first parameter if two (mesh)sets of 
                setpoints are different in the first values
        delay2: delay after setting the second parameter if two (mesh)sets of 
                setpoints are different in the second values
        *param_meas: Parameter(s) to measure at each step or functions that
          will be called at each step. The function should take no arguments.
          The parameters and functions are called in the order they are
          supplied.
        set_before_sweep: if True the outer parameter is set to its first value
            before the inner parameter is swept to its next value.
        enter_actions: A list of functions taking no arguments that will be
            called before the measurements start
        exit_actions: A list of functions taking no arguments that will be
            called after the measurements ends
        before_inner_actions: Actions executed before each run of the inner loop
        after_inner_actions: Actions executed after each run of the inner loop
        before_meas_actions: A list of function taking no argument that will 
            be called before adding results to the datasaver
        do_plot: should png and pdf versions of the images be saved after the
            run.

    Returns:
        The run_id of the DataSet created
    """

    meas = Measurement()
    _register_parameters(meas, (param_set1, param_set2))
    _register_parameters(meas, param_meas, setpoints=(param_set1, param_set2))
    _set_write_period(meas, write_period)
    _register_actions(meas, enter_actions, exit_actions)

    with _catch_keyboard_interrupts() as interrupted, meas.run() as datasaver:
        for i, setps in enumerate(mesh):
            sp1, sp2 = setps[:]
            
            # Set param_set1 to setpoint 1 and sleep if new value
            param_set1(sp1)
            if i > 0: 
                sp1_prior = mesh[i-1][0]
                if sp1 != sp1_prior:
                    time.sleep(delay1)
            
            for action in before_inner_actions:
                action()
            
            # Set param_set2 to setpoint 2 and sleep if new value
            param_set2(sp2)
            if i > 0:
                sp2_prior = mesh[i-1][1]
                if sp2 != sp2_prior:
                    time.sleep(delay2)
        
            # Full delay sleep after setting params to the first set of setpoints
            if i == 0:
                time.sleep( delay1 + delay2 )

            for action in before_meas_actions:
                action()

            datasaver.add_result((param_set1, sp1),
                                 (param_set2, sp2),
                                 *_process_params_meas(param_meas)
                                )

            for action in after_inner_actions:
                action()

            if flush_columns:
                datasaver.flush_data_to_database()

    return _handle_plotting(datasaver, do_plot, interrupted())


def do2d_hyst( param1,start1, stop1, npts1, delay1, 
        param2, start2, stop2, npts2, delay2, *params ) :
    ''' 
    Does two do2d's. First one sweeps param2 from start2 to stop2.
    Second one sweeps param2 from stop2 to start2.

    Args:
        param1: outer loop parameter
        start1: outer loop paremeter start
        stop1: outer loop parameter stop
        npts1 : outer loop number of points
        delay1 : out loop delay
        param2: inner loop parameter (used for hysteresis check)
        start2: inner loop start
        stop2: inner loop stop
        npts2: inner loop number of points
        delay2: inner loop delay
        *params: parameters to measure
    '''
    meas = Measurement()
    meas.register_parameter(param1)
    meas.register_parameter(param2)
    for measure in params:
        meas.register_parameter( measure, setpoints=(param1, param2) )

    meas2 = Measurement()
    meas2.register_parameter(param1)
    meas2.register_parameter(param2)
    for measure in params:
        meas2.register_parameter( measure, setpoints=(param1, param2) )

    with meas.run() as datasaver, meas2.run() as datasaver2 :
        for val1 in np.linspace( start1, stop1, npts1):
            param1(val1)
            time.sleep(delay1)
            for val2 in np.linspace( start2, stop2, npts2 ) :
                param2(val2)
                time.sleep( delay2 )
                output = []
                for p in params :
                    output.append( ( p, p.get() ) )
                datasaver.add_result( (param1, val1), (param2, val2), *output )
            
            for val2 in np.linspace( stop2, start2, npts2 ) :
                param2(val2)
                time.sleep( delay2 )
                output = []
                for p in params :
                    output.append( ( p, p.get() ) )
                datasaver2.add_result( (param1, val1), (param2, val2), *output )
    # _save_image( datasaver )