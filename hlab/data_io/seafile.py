import requests
import io
import pandas as pd


class SeaFile():
    """
    Class to download data from seafile direct download urls.
    Instantiate with a url, and download to_file, to a string variable
    or a pandas dataframe (if it is a CSV)

    Example (not a real URL):
        Instantiate and pass URL
        sf = SeaFile("https://seafile.ist.ac.at/f/7548c47d65654ef884fa/?dl=1")
        
        Save to pandas dataframe
        df = sf.to_csv_pd()

        Save to local file
        sf.to_file("temp.csv")

        Save to string
        datastr = sf.to_str()

    TODO: Autodetect file type by header and provide automatic casting
    to csv, text etc. 

    """

    def __init__(self, url):
        """
        One instance for each file with a download URL

        Args:
            url (str) : direct download link. 
        
        """
        self.url = url
        self._bytes = False
        

        
    def to_file(self, filepath):
        """
        Sore the data in a file

        Args:
            filepath (str): path to the file. example: ~/data/myfile.csv 

        """

        if self._get_bytes():
            print("Data download done !")
            with open(filepath,'wb') as out:
                out.write(self._bytes.read())
            print("File saved!")
            self._bytes = False
        else:
            self._bytes = False
            print("Something went wrong downloading your file")

    def to_str(self):
        """
        Sore the data in a string variable

        Returns:
            str : The file as a utf8 formatted string.

        """
        if self._get_bytes():
            print("Data download done !")
            return self._bytes.read().decode('utf8')
        else:
            self._bytes = False
            print("Something went wrong downloading your file")

    def to_csv_pd(self):
        """
        Sore csv data in a pandas dataframe

        Returns:
            df (pd.dataframe) : Pandas dataframe of the file

        """
        
        return pd.read_csv(self.url)


    def _get_bytes(self):
        response = requests.get(self.url, stream=True)
        self._bytes = io.BytesIO(response.content)
        return True