# VNAs2p.py
# Containing functions to read in .s2p from R&S VNA
import numpy as np
import matplotlib.pyplot as plt
import glob
import os

from hlab.data_io.dstruct import data1d

def plot( mag : data1d, phase: data1d ) :
    ''' plot magnitude and phase on same x axis
    '''
    fig, (ax1, ax2) = plt.subplots( 2, 1, figsize=[16,9], sharex=True )

    mag.plot( ax1 )
    ax1.set_xlabel( None )
    phase.plot( ax2 )
    plt.tight_layout()
    return fig, ax1, ax2

def plot_matrix( s ) :
    ''' s parameters in a 2x2 grid
    returns fig, [ lh axes ], [rh axes]
    '''
    fig, ax2d = plt.subplots( 2, 2, figsize=[12,7] )
    ax_lhs = ax2d.flatten()
    ax_rhs = [ ax.twinx() for ax in ax_lhs ]

    for s, ax1, ax2 in zip( s, ax_lhs, ax_rhs ) :
        mag = s[0]
        phase = s[1]
        mag.plot( ax1 )
        phase.plot( ax2, color='orange' )
        ax2.set_ylim( (-200,360) )

    plt.tight_layout()

    return fig, ax_lhs, ax_rhs

def plot_append( data, plot, legend=None ) :
    ''' Append data (output of loader) to plot (output of plot()).
    Put the legend on all plots '''
    mag, phase = data
    _, ax1, ax2 = plot

    mag.plot( ax1 )
    ax1.set_xlabel( None )
    phase.plot( ax2 )

    if legend is not None :
        ax1.legend( legend )
        ax2.legend( legend )
    plt.tight_layout()

class magphase_loader( ) :
    ''' Class for loading s2p.
    Returns a .load() returns a tuple (s11, s21, s12, s22).
    Contains loader and plotter helper functions.
    The purpose of the clas is to wrap up measurement-specific metadata (file path, format, etc)
    '''

    def __init__( self, dpath ) :
        ''' dpath e.g. '%s/black_hole/SampleRecords/3/20191010_Sample_03_RT_4K_S11' % seafile
        mag_col/phas_col are columns that will be plotted as magnitude and phase
        self.mag_label, phase_label, f_label will be used to create labels for data1d objects
        '''
        
        self.dpath = dpath
        self.mag_label = "$|S_{%s}|$ (dB)"
        self.phase_label = "$\\angle S_{%s}$ (deg)"
        self.f_label = "Frequency (Hz)" 
        self.s = [ '11', '21', '12', '22' ] # configure indices
        self.delay_start = 2e9 # range for auto_delay on phase
        self.delay_stop = 4e9


    def load( self, file, skiprows=5, unwrap=False, pre_f=None ) :
        ''' Load file with np.loadtxt 
        auto_delay = True causes us to auto-gess the delay
        file can include regular expressions to be used by glob.glob 
        pre_f is a function to apply to the (mag, phase) tuple before returning
        for instance, it could be lin mag to db conversion
        TODO: deprecate unwrap in favor of pre_f
        '''

        full =  '%s/%s' % ( self.dpath, file )
        matched = glob.glob( full )[0]
        data = np.loadtxt( matched, skiprows=7 )
        
        f = data[ :, 0 ] # frequency
        sparams = data[ :, 1 :: ].transpose() # s parameters

        phases = sparams[ 1 :: 2, : ] # unwrap the phase
        if unwrap :
            unwrp = [ self.auto_delay( f, x ) for x in phases ]
        else :
            unwrp = phases

        # pair up mag/phase/strings
        m_p_s = zip( sparams[ :: 2, : ], unwrp, self.s )
        s = [ self._array2s( f, *args ) for args in m_p_s ]

        if pre_f is not None :
            s = [ pre_f( d ) for d in s ]

        return s


    def auto_delay( self, f, phase ) :
        ''' Automatically unwrap the phase
        input should be in degrees.
        Returns the unwrapped output 
        Frequency should be in Hz. 
        Unwrap in range set by self.delay_start, self.delay_stop'''

        unwrp = np.unwrap( phase * np.pi / 180 )
        d1d = data1d( unwrp, 'phase', f, 'freq' )
        trimmed = d1d( self.delay_start, self.delay_stop )
        x = trimmed.x
        p = np.polyfit( x, trimmed, 1 )
        sub = unwrp - p[0] * f - p[1]
        mod = np.mod( sub + np.pi, 2 * np.pi ) - np.pi
        # return phase
        return mod * 180 / np.pi

    def _array2s( self, f, mag, phase, s : str ) :
        ''' convert arrays to list of data1ds [mag, phase]
        t is a (mag, phase) tuple
        '''
        d1 = data1d( mag, self.mag_label % s, f, self.f_label )
        d2 = data1d( phase, self.phase_label % s, f, self.f_label )
        return [ d1, d2 ]

    def ls( self, search='*.s2p' ) :
        ''' get list of all s1p files in dpath '''
        os.chdir( self.dpath )
        file_list = glob.glob( search )
        return sorted( file_list )


    def lp( self, *args, idx=1, unwrap=True, legend=None, pre_f=None ) :
        ''' lp( n1, n2, n3, ... )
        Shortcut to load and plot n1, n2, n3, ...
        Returns [ fig, data1, data2, data3, ... ]
        Plot s2p index idx
        pre_f is a function to apply to the ( mag, phase ) tuple e.g. mag/db conversion
        '''
  
        # make a list of all the data sets
        f = lambda n : self.load( "%02d*" % n, unwrap=unwrap, pre_f=pre_f )[ idx ]
        data_list = [ f( n ) for n in args ]
            
        fig = plot( *data_list[0] ) # plot the first one

        for data in data_list[ 1 : ] : # plot the rest of the data
            plot_append( data, fig, legend=None )
            
        if legend is None : # add the legend
            if len( args ) > 1 : # no legend for single plot
                legend = args
                fig[1].legend( legend )
                fig[2].legend( legend )
        else :
            fig[1].legend( legend )
            fig[2].legend( legend )
        
        return [ fig, *data_list ]

    def lp2( self, n1, n2, unwrap=True, legend=None ) :
        ''' Shortcut to load and plot traces n1, n2 '''
        print( "lp2 is deprecated. Use lp( n1, n2 ) instead" )
        return self.lp( n1, n2, unwrap=unwrap, legend=legend )

    def plot_all( self, search='*.s1p' ) :
        ''' Automatically plot all s2p files in dpath 
        search: search string 
        Returns a list of files in the current directory
        TODO: implement this'''
        pass
        # files = self.ls( search )
        # datas = [ self.load( f ) for f in files ]

        # for f, d in zip( files, datas ) :
        #     fig, _, _ = plot( *d ) 
        #     fig.suptitle( f )
        #     plt.tight_layout()
    
        # return files