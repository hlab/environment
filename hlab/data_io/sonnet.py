import numpy as np
import matplotlib.pyplot as plt
from re import split
from hlab.data_io.dstruct import data1d_cmplx
from numpy.core.defchararray import index
from scipy.optimize.minpack import check_gradient


def get_first_index_if_str_included(strs, substr):
    """Search through an array of string and return the first index of the
    element that includes a substring"""
    first_index = None
    list_of_appearance_index = \
        [i for i, s in enumerate(strs) if substr in s]
    if len(list_of_appearance_index) > 0:
        first_index = list_of_appearance_index[0]
    return first_index


class SonnetLoaderCadence:
    """Parser to load Cadence-type data from Sonnet. It seeks the line
    that includes 'format' to extract data tags and translate the rest to
    2d numpy array.
    self.data_format = [tag0, tag1, tag2...]
    self.data_array = 2d_array_of_data[ col_index : frequency_index ]
    """
    def __init__(self, cadence_pathname : str):
        with open( cadence_pathname, 'r' ) as infile:
            self.rawlines = [ line for line in infile ]

        self.data_format, lnum = self.get_dataformat(self.rawlines)
        self.data_array = self.get_np_data_array(self.rawlines, lnum+1)
        self.header = self.rawlines[:lnum+1]
        
        self.freq_idx = \
                get_first_index_if_str_included(self.data_format, 'freq')


    def get_dataformat(self, rawlines : list):
        """Seeks line that includes 'format' and return a dict of 
        {'Format' : ['tag0', 'tag1', 'tag2',...]} and the line number.
        """
        splitted = None
        for linenum, line in enumerate(rawlines):
            if 'format' in line:
                splitted = [x.strip() for x in split('\t', line)]
                break
        if splitted is None:
            print("Can't find data format")
        data_format = splitted[1:]
        return data_format, linenum

    def get_np_data_array(self, rawlines : list, start_lnum : int):
        """Read lines to 2d numpy array from rawlines from index start_lnum+1
        """
        numdata_list = []
        for i, line in enumerate(self.rawlines[start_lnum:]):
            try:
                splitted = split(',|:', line)
                numdata_list.append( list(map(float,splitted)) )
            except:
                print("EOF or error at {:d}".format(start_lnum+i))
                break
        data_array = np.array(numdata_list).T
        return data_array

    def __len__(self):
        """Should return length of a trace (=numpoints of frequency)"""
        return len(self.data_array[0])

    def __getitem__(self, col : int =0):
        """Return the corresponding column"""
        return self.data_array[col]

    def get_d1dcmplx_from_cols( self, mag_db_col : int , phase_rad_col : int, 
                freq_col : int = None, ylabel='Sparam', xlabel='Frequency' ) \
                    -> data1d_cmplx :
        if freq_col is not None:
            _fcol = freq_col
        else:
            _fcol = self.freq_idx
        return data1d_cmplx( mag_db = self.__getitem__(mag_db_col),
                        phase_rad = self.__getitem__(phase_rad_col)/180*np.pi,
                        x = self.__getitem__(_fcol), xlabel='Frequency',
                        ylabel=ylabel )

    def get_S(self, key='S11') -> data1d_cmplx:
        """Assuming exported data is of format freq, S11(db,deg), S12...
        Return data1d_cmplx obj construction of the corresponding S params"""
        _idx = get_first_index_if_str_included(self.data_format, key)
        return self.get_d1dcmplx_from_cols( mag_db_col = _idx*2-1,
                                        phase_rad_col = _idx*2,
                                        freq_col = self.freq_idx,
                                        ylabel=self.data_format[_idx],
                                        xlabel='Frequency' )



class SimData:
    """Class to hold a full sonnet simulation curve output.

    An instance of this class gives an interactive data object to analyse the
    .csv output of a sonnet Em simulation i.e. what you get when you plot a 
    bunch of curves and right click --> "all curves to spreadsheet"

    Example:
        sd = SimData("./resonator.csv")
    
        Note that the iterator is implemented for this class, so you can browse through the
        data like so:

        for sim in sd:
            print(sim.info)

    TODO: Differentiate between the same scan with different parameters and different curves. May not be
    possible because sonnets formatter behaves like a drunk money. 
    """
    def __init__(self, path):
        lines = []
        with open(path, 'r') as file:
            lines = [line for line in file] 
        self.datablocks = {}
        datablock = []
        for line in lines:
            if line != '\n':
                datablock.append(line)
            else:
                bl = SimDataBlock(datablock)
                self.datablocks["".join(bl.info.strip())] = bl
                datablock = []
                
    def __len__(self):
        return len(self.datablocks)
    
    def __getitem__(self, position):
        return [val for key,val in self.datablocks.items()][position]
        
class SimDataBlock:
    """Class to hold a sonnet simulation curve output for a single variable or equation / parameter.

    In practice, the SimDataBlock holds one of the curves you see in the Sonnet simulation output. It gets
    instantiated by SimData. 
    It puts the frequencies into the x_data propery and whatever is recorded in y_data.
    x_label and y_label tell you what's inside. 

    Example:
        sd = SimData("./resonator.csv") 
        sd[0] --> the SimDataBlock instance
        for instance try:
        sd[0].plot()

    
    """ 
    def __init__(self, datablock):
        self.info = datablock[0].strip()
        self.x_label = datablock[1].split(",")[0].strip()
        self.y_label = datablock[1].split(",")[1].strip()
        try:
            self.x_data = np.array([float(x.split(",")[0].strip()) for x in datablock[2:]])
            self.y_data = np.array([float(x.split(",")[1].strip()) for x in datablock[2:]])
        except ValueError:
            print("Something went wrong parsing the datablock: \n"+self.info)
        
    def __len__(self):
        return np.max(self.x_data.shape)
    
    def plot(self, output=False):
        if output:
            fig = plt.figure()
            plt.plot(self.x_data, self.y_data)
            plt.ylabel(self.y_label)
            plt.xlabel(self.x_label)
            plt.title(self.info)
            return fig
        else:
            plt.plot(self.x_data, self.y_data)
            plt.ylabel(self.y_label)
            plt.xlabel(self.x_label)
            plt.title(self.info)
            

class SXYZ:
    """
    Class to hold S, X, Y, Z parameter output from Sonnet.

    Right cloick on the plot in sonnt and choose S, Y, Z parameter file. Note
    that you can only choose outputs from one parameter combination, if you
    have run a parameter sweep and only output one of the 4 (S,Y,Z or X). 

    Args:
        info list(str): Information from the header split up in lines
        columns list(str):  Column names of the data
        data (dict{str:np.array}):  Stores the raw data as np array. Careful, dicts are
                                    not lists, see below

    Example:
        sxyz = SXYZ("./resonator.csv")

        # See what's inside 
        sxyz.columns

        # See file info
        sxyz.info

        # Make a quick frequency / parameter plot

        sxyz.plot()
    
    Note that the iterator is also implemented here in def __getitem__ so you can cycle through the data
    columns with:
    for name, values in sxyz:
        print(name)
        print(np.max(values))

    The iteator is "dictionary safe", because it uses the columns as indices, so if you want to hardcode
    columns by index (though this is not recommended because the name in the dictionary makes the code
    more readable), DO NOT do something like this [values for name, values in szyz.data.items()][3] to get
    the fourth column. Dictionaries are not ordered, so the column numbers straight from the dict may change. 
    
    """

    def __init__(self, path):
        with open(path, 'r') as file:
            lines = [line for line in file] 
        info = []
        for cnt, line in enumerate(lines):
            if "Frequency" not in line:
                info.append(line.strip())
            else:
                columns = line.strip().split(",")
                break
        
        self.info = info    
        self.columns = columns
        data = np.genfromtxt(path, skip_header=cnt+1, delimiter=",")
        self.data = {}
        for cnt, val in enumerate(columns):
            self.data[val] = data[:,cnt]
        
    def __getitem__(self, position):
        return self.columns[position], self.data[self.columns[position]]
                
    def __len__(self):
        return len(self.columns)

    def _data_at_idx(self, idx):
        return np.array([val[idx] for name, val in self])     

    def at_frq(self, frequency, ):
        """Return a slice of data at the chosen frequency

        It will also return a slice of data if the exact frequency is not in the dataset
        and instead return the data for the frequency closest to it

        Args:
           frequency (float): frequency where you want to see the data

        Returns:
            (numpy array): Slice of the data, including frequency closest to arg frequency

        """
        index = np.argmin(np.abs(self[0][1]-frequency))
        return self._data_at_idx(index)


    def plot(self, idx=None, output=False ):
        ''' Plot the contents of SXYZ
        idx = [ 1, 2, 3 ] controls which indices are plotted. By default, plot everything.
        '''
        
        if idx is None : # By default, plot everything
           idx = range( len( self.columns ) )

        if output :
            fig = plt.figure()
            self.__plotIDX( idx )
            plt.show()
            return fig
        else :
            self.__plotIDX( idx )

    def __plotIDX( self, idx ) :
        ''' Helper function for plot: does the plotting for indices named in the list idx=[1,3,7]
        '''
        for cnt, val in enumerate( self ):    
            if ( cnt > 0 ) and ( cnt in idx ) :
                plt.plot(self[0][1],val[1], label=val[0])
        plt.xlabel(self.columns[0])
        plt.legend()
