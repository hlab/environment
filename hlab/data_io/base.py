from qcodes import Parameter
from qcodes.instrument.parameter import expand_setpoints_helper
from qcodes.dataset.dependencies import InterDependencies, InterDependencies_
from qcodes.dataset.data_set import DataSet
from qcodes.dataset.measurements import Measurement
from qcodes.utils.validators import Arrays


class DataBlock(Parameter):
    def __init__(self, name, unit, data_source):
        super.__init__(name, unit=unit, instrument=data_source, set_cmd=None, vals=Arrays())
        