# VNAs1p.py
# Containing functions to read in .s1p from R&S VNA
import numpy as np
import matplotlib.pyplot as plt
import glob
import os

from hlab.data_io.dstruct import data1d

def readReIm(filename):
    """Read s1p file of S11 and convert to 3 array of freq, Re and Im"""
    lines = []
    with open(filename, 'r') as infile:
        listlines = [line.rstrip() for line in infile if line.strip()]
        lines = [line.split() for line in listlines]
    freq = np.array([])
    res11 = np.array([])
    ims11 = np.array([])
    for line in lines[5:]:
        freq = np.append( freq, float(line[0]) )
        res11 = np.append( res11, float(line[1]) )
        ims11 = np.append( ims11, float(line[2]) )
        
    return freq, res11, ims11

def plot( mag : data1d, phase : data1d ) :
    ''' Plot mag/phase in a 2x1 window
    '''
    fig, (ax1, ax2) = plt.subplots( 2, 1, sharex=True )
    mag.plot( ax1 )
    phase.plot( ax2 )
    ax1.set_xlabel( None )
    plt.tight_layout()

    return ( fig, ax1, ax2 )

def plot_append( data, plot, legend="" ) :
    ''' Append data (output of loader) to plot (output of plot()).
    Put the legend on all plots '''
    mag, phase = data
    _, ax1, ax2 = plot

    mag.plot( ax1 )
    ax1.set_xlabel( None )
    ax1.legend( legend )

    phase.plot( ax2 )
    ax2.legend( legend )
    plt.tight_layout()

class magphase_loader() :
    ''' Class for loading s1 magnitude and phase
    Contains loader and plotter helper functions.
    The purpose of the clas is to wrap up measurement-specific metadata (file path, format, etc)
    '''

    def __init__( self, dpath, f_col = 0, mag_col=1, phase_col=2 ) :
        ''' dpath e.g. '%s/black_hole/SampleRecords/3/20191010_Sample_03_RT_4K_S11' % seafile
        mag_col/phas_col are columns that will be plotted as magnitude and phase
        self.mag_label, phase_label, f_label will be used to create labels for data1d objects
        '''
        
        self.dpath = dpath
        self.f_col = f_col
        self.mag_col = mag_col
        self.phase_col = phase_col
        self.mag_label = "$|S_{11}|$ (dB)"
        self.phase_label = "$\\angle S_{11}$ (deg)"
        self.f_label = "Frequency (Hz)" 


    def load( self, file, skiprows=5 ) :
        ''' Load file with np.loadtxt '''

        full =  '%s/%s' % ( self.dpath, file )
        matched = glob.glob( full )[0]
        data = np.loadtxt(  matched, skiprows=5 )
        
        f = data[ :, self.f_col ]
        mag = data[ :, self.mag_col ]
        phase = data[ :, self.phase_col ]

        mag_data = data1d( mag, self.mag_label, f, self.f_label )
        phase_data = data1d( phase, self.phase_label, f, self.f_label )

        return ( mag_data, phase_data )

    def ls( self, search='*.s1p' ) :
        ''' get list of all s1p files in dpath '''
        os.chdir( self.dpath )
        file_list = glob.glob( search )
        return sorted( file_list )

    def plot_all( self, search='*.s1p' ) :
        ''' Automatically plot all s1p files in dpath 
        search: search string 
        Returns a list of files in the current directory'''

        files = self.ls( search )
        datas = [ self.load( f ) for f in files ]

        for f, d in zip( files, datas ) :
            fig, _, _ = plot( *d ) 
            fig.suptitle( f )
            plt.tight_layout()
    
        return files