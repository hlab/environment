# hlab

## Manual installation

Manual installation steps:

1. Install anaconda
2. If you are you are using Windows, install gitbash

If you have a Mac, you probably have git already. Try typing 'git' in the command line to check. 
If you don't have it, ask for help.

It will be nice to make gitbash conda-aware:
```
echo ". /c/Anaconda3/etc/profile.d/conda.sh" >> ~/.bash_profile
```
You may have to adjust the path to conda.sh depending on your conda installation.
For instance /c/Users/<<me>>/Anaconda3/etc/profile.d/conda.sh.

3. Clone repos into ~/src/ (you may want to do this through ssh for easy pushing later on):

 [Note : On Mac, repositories are cloned to your home directory by default. It would be a good idea to make a directory '\src' and then clone.]
* https://git.ist.ac.at/hlab/environment


Cloning the environment can be done in two ways : through SSH or through https. The 'hacker' way to do it is through SSH.

**For SSH:**

1) Generate a SSH key pair. It would be a good idea to go with key type ED25519. 
   Instructions on https://git.ist.ac.at/help/ssh/index.md#generate-an-ssh-key-pair

2) Add a SSH key to your GitLab account https://git.ist.ac.at/help/ssh/index.md#add-an-ssh-key-to-your-gitlab-account

3) Check that you can connect https://git.ist.ac.at/help/ssh/index.md#verify-that-you-can-connect

These are the broad steps for generating and adding a SSH key. In case of any trouble shooting, refer to 
https://git.ist.ac.at/help/ssh/index.md#troubleshooting

4) Then go to Terminal and clone with SSH.

**For https :**

1) Go to terminal and clone with https

Then, clone the following repos :

* https://github.com/labist/Qcodes
* https://github.com/labist/Qcodes_contrib_drivers (required for data acquisition)
* https://github.com/labist/plottr

4. Install evironment:
```bash
conda env create -f ~/src/environment/environment.yml
conda activate hlab
``` 
Note that this will auatomatically install qdev-warpper, broadbean, chickpea, and plottr directly from git.

5. Install packages:
```bash
pip install -e ~/src/environment
pip install -e ~/src/Qcodes
pip install -e ~/src/Qcodes_contrib_drivers
pip install -e ~/src/plottr
``` 

6. Clone and install project-specific repos. You're ready to measure!
Packages in project-specific repos may need to be installed in a similar way to the packages in step (5). For example:

```bash
pip install -e ~/src/membranes
pip install -e ~/src/jj_chains
``` 

They are located on git.ist.ac.at. For instance https://git.ist.ac.at/hlab/jj_chains.

7. Install seafile desktop drive client from https://www.seafile.com/en/download/. 

When you first import hlab, a file called ~/.hlab/hlab.json is created. Make sure it points to the correct seafile path.
For instance, for a windows user withe the seafile sync client it should read:
```
{
    "data":{
        "default_path" : "~/Documents/Seafile"
    },
    "scripts":{
        "default_path" : "~/src"
    }
}
```
## Auto-installation
Scripts are provided that attempt to automatically install everything you need. They are likely out of date.

### Windows

If you already have a version of python / anaconda etc on your machine, uninstall it.

Download the `install.ps1` script.

In the script, you can edit the preferred code directory. By default it's `$Home/src`. 

Run it by right clicking on the script and pressing `run with Powershell`. 

Done ❤️

From now on, when you work with python for hlab, open the anaconda power shell and
type

```bash
conda activate hlab
``` 

before you start a notebook.

### Mac / Linux 

Download and install the latest version of miniconda with python 3.xx for your system, from https://docs.conda.io/en/latest/miniconda.html or with homebrew. 

Download the install.ls script

Run the by opening the terminal, navigating to the location of the script and running 

```bash
sh install.ls
```

Done ❤️

From now on, when you work with python for hlab, open the terminal and
type:

```bash
conda activate hlab
```

## Updating hlab

To keep your environment up to date, go to the environment folder and type:

```bash
git pull
``` 

and then 

```bash
conda env update -f environment.yml
conda activate hlab
``` 

## Adding your new dependencies to hlab

If you install packages which are not already part of hlab to make your contribution to the environment run, you have to update the hlab environment.yml file. Ideally, you do this in a separate commit with justifying the additional dependencies. 
To create the updated .yml file, navigate into the environment folder and run:

```bash
conda env export --no-builds | grep -v "prefix" > environment.yml
```








