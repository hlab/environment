# Installation script for the hlab environment. Right click on the file and run it with the powershell.
# Based on the instal script for qdev wrappers on https://github.com/qdev-dk/qdev-wrappers.

#Set directory for code

$code_dir = "$HOME\src"


# Set URLs for enviornments
$hlab_url = "https://git.ist.ac.at/hlab/environment" 
$qcodes_url = "https://github.com/labist/Qcodes"
$qdev_wrappers_url = "https://github.com/qdev-dk/qdev-wrappers"

#Optional qdev wrapper dependencies. Comment this out if you don't want to install them.
$broadbean_url = "https://github.com/QCoDeS/broadbean"
$chickpea_url = "https://github.com/nataliejpg/chickpea"
$plottr_url = "https://github.com/data-plottr/plottr"


# Set-ExecutionPolicy -ExecutionPolicy Bypass -Scope Process
$miniconda_url = "https://repo.continuum.io/miniconda/Miniconda3-latest-Windows-x86_64.exe"
$miniconda_exe = "$env:TEMP\miniconda.exe"
$env_bat = "$env:TEMP\create_qcodes_env.bat"
$conda_install_bat = "$env:TEMP\conda_install.bat"
$miniconda_install_path = "$HOME\miniconda\"
$git_exe = "$env:TEMP\git.exe"
$git_url= 'https://git-scm.com/download/win'



# install miniconda and update
Write-Host 'downloading miniconda, please wait'
(New-Object System.Net.WebClient).DownloadFile($miniconda_url, $miniconda_exe)| Out-Host
Write-Host 'installing miniconda'
&$miniconda_exe /InstallationType=JustMe /AddToPath=1 /S "/D=$miniconda_install_path" | Out-Host
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User") 
Write-Host 'updating miniconda'
conda update -y conda
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

# download git
Write-Host 'download latest version of git'
$html = Invoke-WebRequest -Uri $git_url
$e = ($html.ParsedHtml.getElementsByTagName('a') | Where{ $_.innerText -eq '64-bit Git for Windows Setup' } )
$git_setup_url = $e.attributes.getNamedItem('href').nodeValue()
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest -Uri $git_setup_url -OutFile $git_exe

# install git
Write-Host 'installing git'
&$git_exe "/SILENT"| Out-Host
$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")

mkdir $code_dir

cd $code_dir

# clone repos
Write-Host 'cloning qcodes relvant repos'
git clone $qcodes_url
git clone $hlab_url
git clone $qdev_wrappers_url

if($broadbean_url){
    git clone $broadbean_url
}
if($chickpea_url){
    git clone $chickpea_url
}

if($plottr_url){
    git clone $plottr_url
}


# create qcodes virtualenv
Write-Host 'creating the virtual environment'
conda env create -f "$code_dir\environment\environment.yml"
Write-Host 'activating the virtual environment'

conda activate hlab

#$install_cmd = 'CALL activate qcodes
#CALL conda install -y spyder
#CALL conda install -y jupyter
#CALL conda install -y scipy'
#$install_cmd | Set-Content $conda_install_bat
#cmd /c $conda_install_bat

Write-Host 'installing qcodes'

$env_cmd = 'CALL activate hlab
CAll pip install -e %UserProfile%\src\Qcodes
CALL pip install -e %UserProfile%\src\environment'
$new_line = "`r`n"
if($qdev_wrappers_url){
    $env_cmd += $new_line + "CALL pip install -e $code_dir\qdev-wrappers"
}
if($broadbean_url){
    $env_cmd += $new_line + "CALL pip install -e $code_dir\broadbean"
}
if($chickpea_url){
    $env_cmd += $new_line + "CALL pip install -e $code_dir\chickpea"
}
if($plottr_url){
    $env_cmd += $new_line + "CALL pip install -e $code_dir\plottr"
}
$env_cmd | Set-Content $env_bat
cmd /c $env_bat
# &$env_bat | Out-Host
