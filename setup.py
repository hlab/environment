import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="hlab",
    version="0.0.1",
    author="Johannes Beil",
    author_email="johannesbeil@gmail.com",
    description="Higginbotham group analysis and control stack",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ist.ac.at/hlab/environment",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: let's see about that",
        "Operating System :: OS Independent",
    ],
)