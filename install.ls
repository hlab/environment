#Script to install the hlab enviornment
#Edit this if it's different for you
code_dir=$HOME/src
bash_script=~/.bashrc
echo $USER

mkdir $code_dir

hlab_url="https://git.ist.ac.at/hlab/environment"
qcodes_url="https://github.com/labist/Qcodes"
qdev_wrappers_url="https://github.com/qdev-dk/qdev-wrappers"

conda_dir=$(which anaconda | grep -Eo .+anaconda.)

#Optional qdev wrapper dependencies. Comment this out if you don't want to install them.
broadbean_url="https://github.com/QCoDeS/broadbean"
chickpea_url="https://github.com/nataliejpg/chickpea"

cd $code_dir

echo "Cloning / installing hlab"
git clone $hlab_url
cd environment
git pull

echo "Upgrade conda"
conda update -n base conda
conda env create -f environment.yml
echo "Activate conda environments"
echo ". $conda_dir/etc/profile.d/conda.sh" >> $bash_script
echo "conda activate" >> $bash_script
. $conda_dir/anaconda3/etc/profile.d/conda.sh
conda activate hlab

echo "Upgrade pip, just in case"
pip install --upgrade pip
pip install .

cd $code_dir

echo "Cloning /installing qcodes"
git clone $qcodes_url
cd Qcodes
git pull
pip install -e .
cd $code_dir


echo "Cloning qdev wrappers"
git clone $qdev_wrappers_url
cd qdev-wrappers
git pull
pip install -e .
cd $code_dir



if [ -n "$broadbean_url" ]
then
    git clone $broadbean_url
    cd broadbean
    git pull
    pip install -e .
    cd $code_dir
fi
if [ -n "$chickpea_url" ]
then
    git clone $chickpea_url
    cd chickpea
    git pull
    pip install -e .
    cd $code_dir
fi

